<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
date_default_timezone_set('America/Panama');

//define the base app Path
define('BASE_PATH', dirname(__FILE__) . '/../app/');

//start session if not active
if (session_status() !== PHP_SESSION_ACTIVE) {session_start();}

//include required files
require_once '../app/lib/config.php';
require_once '../app/lib/loader.php';
require_once '../app/lib/database.php';
require_once '../app/lib/dispatcher.php';
require_once '../app/controllers/base.php';
require_once '../app/models/base.php';
require_once '../app/lib/layout.php';
require_once '../app/lib/view.php';


Config::run();
Dispatcher::run();



