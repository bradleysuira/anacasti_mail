<?php
class ClientesController extends BaseController
{	
	public $layout = 'layouts/default';
	protected $clientes;
	protected $paises;

	public function __construct()
	{
		Loader::resource('models', 'cliente');
		Loader::resource('models', 'pais');
		$this->clientes = new ClienteModel();
		$this->paises = new PaisModel();
	}
	
	public function getIndex()
	{	
		$data = array();
		try {
			Loader::resource('lib', 'paginator');

			$count = $this->clientes->count();
			$offSet = 10;
			$totalPages = ceil($count / $offSet);
			$page = ( is_null($this->queryString('page')) 
					  || ( !is_numeric($this->queryString('page')))
					  || ( $this->queryString('page') < 0 )
					  || ( $this->queryString('page') > $totalPages )
					 ) ? 1: $this->queryString('page');

			$limit = $offSet * ($page -1 );			
			$data['clientes'] = $this->clientes->getAll($limit, $offSet, "id desc");	
			
			$pages = Paginator::make($offSet , $page, $count);
			$data['paginator'] = $pages;			

		} catch (Exception $e) {
			$data['paginator'] = null;
			$data['messages'] = "Error al obtener la información de los clientes . "  . $e->getMessage();
		}						

		View::make('clientes/index', $data);			
	}	

	public function getCrear()
	{

		$data = array();
		try {

			$data['paises'] = $this->paises->getAll();		
		} catch (Exception $e) {
			$data['messages'] = "Error al obtener la información de los paises . "  . $e->getMessage();
		}						
		View::make('clientes/crear', $data);								
	}

	public function postCrear()
	{
		$data = array();
		$errors = array();
		$messages = "";
		try {		
			$ruc = $this->post('ruc');
			$name = $this->post('name');
			$email = $this->post('email');
			$active = $this->post('active');
			if(empty($ruc))
				$errors[] = 'ruc';
			if(empty($name))
				$errors[] = 'name';
			if(empty($email))
				$errors[] = 'email';
			$data['errors'] = $errors;		

			if(count($errors) == 0) {
				$formData = $this->post();
				if(empty($active))
					$formData['active'] = "0";

				$resultId = $this->clientes->add($formData);
				if($resultId > 0)
				{
					Dispatcher::redirectToAction('envios','crear', array('cliente'=>$resultId));
				}
				else{
					$data['paises'] = $this->paises->getAll();	
					$data['messages'] = "Error al crear el cliente, favor verifique e intente nuevamente.";	
				}

			}else{

				$data['paises'] = $this->paises->getAll();	
				$data['messages'] = "Por favor verifique los campos obligatorios";
			}
		} catch (Exception $e) {
			$data['messages'] = "El cliente no pudo ser creado por un error en la aplicación. "  . $e->getMessage();						
		}
		View::make('clientes/crear', $data);
	}

	public function getEditar()
	{
		$data = array();
		try {
			if(is_null($this->queryString('id')) || $this->queryString('id') < 1){
				$this->notFound();
			}else{
				$cliente = $this->clientes->find($this->queryString('id'));				
				if(is_null($cliente))
				{
					$this->notFound();
				}else{					
					$data['paises'] = $this->paises->getAll();							
					$data['cliente'] = $cliente;
					View::make('clientes/editar', $data);
				}	
			}						
		} catch (Exception $e) {
			$data['messages'] = "Error al intentar editar el cliente. "  . $e->getMessage();						
			$this->notFound();
		}		
	}

	public function postEditar()
	{
		$data = array();
		$errors = array();
		$messages = "";
		try {		
			if(is_null($this->queryString('id')) || $this->queryString('id') < 1){
				$this->notFound();
			}else{
				$cliente = $this->clientes->find($this->queryString('id'));				
				if(is_null($cliente))
				{
					View::make('error/notFound');
				}else{					
					if(is_null($this->post('ruc')))
						$errors[] = 'ruc';
					if(is_null($this->post('name')))
						$errors[] = 'name';
					if(is_null($this->post('email')))
						$errors[] = 'email';
					if(count($errors) > 0 ) {
						$data['paises'] = $this->paises->getAll();			
						$data['errors'] = $errors;	
						View::make('clientes/editar', $data);						
					}
					else{

						$formData = $this->post();					
						$this->clientes->edit($formData, $this->queryString('id'));
						Dispatcher::redirectToAction('clientes','index'); 									
					}
				}	
			}			
		} catch (Exception $e) {
			$data['messages'] = "El cliente no pudo ser modificado por un error en la aplicación. " . $e->getMessage();						
			View::make('clientes/editar', $data);
		}		
	}

	public function getEliminar()
	{
		$data = array();
		try {
			if(is_null($this->queryString('id')) || $this->queryString('id') < 1){
				$this->notFound();
			}else{
				$cliente = $this->clientes->find($this->queryString('id'));				
				if(is_null($cliente))
				{
					$this->notFound();
				}	
				$data["cliente"] = $cliente;
			}						
		} catch (Exception $e) {
			$data['messages'] = "Error al intentar eliminar el cliente. "  . $e->getMessage();						
		}

		View::make('clientes/eliminar', $data);
	}

	public function postEliminar()
	{
		$data = array();				
		try {		
			if(is_null($this->queryString('id')) || $this->queryString('id') < 1){
				$this->notFound();
			}else{
				$cliente = $this->clientes->find($this->queryString('id'));				
				if(is_null($cliente))
				{
					$this->notFound();
				}else{							
					$this->clientes->delete($this->queryString('id'));
					Dispatcher::redirectToAction('clientes','index'); 													
				}	
			}
			
		} catch (Exception $e) {
			$data['messages'] = "El cliente no pudo ser eliminado por un error en la aplicación. " . $e->getMessage();						
		}

		View::make('clientes/eliminar', $data);
	}


}