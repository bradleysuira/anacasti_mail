<?php
class EnviosController extends BaseController
{
	public $layout = 'layouts/default';
	protected $clientes;
	protected $envios;
	protected $plantillas;

	public function __construct()
	{
		Loader::resource('models', 'cliente');
		Loader::resource('models', 'envio');
		Loader::resource('models', 'plantilla');

		$this->clientes = new ClienteModel();
		$this->envios = new EnvioModel();
		$this->plantillas = new PlantillaModel();
	}

	public function getCrear()
	{
		$data = array();
		try {

			$idcliente = $this->queryString('cliente');
			if(is_null($idcliente))
				throw new Exception("Cliente no válido", 1);
			if($idcliente < 1)
				throw new Exception("Cliente no válido", 1);

			$cliente = $this->clientes->find($idcliente);
			
			if(is_null($cliente)){
				$this->notFound();
			}
			else{
				$envio = $this->envios->findByClient($cliente->id);
				if(is_null($envio)){
					$data['cliente'] = $cliente;
					$data['plantillas'] = $this->plantillas->getAll();
					View::make('envios/crear', $data);
				}else{
					Dispatcher::redirectToAction('envios','editar', array('cliente'=>$cliente->id));
				}
			}			

		} catch (Exception $e) {
			$data['messages'] = "Error al obtener la información de los paises . "  . $e->getMessage();
			View::make('envios/crear', $data);	
		}		
	}

	public function postCrear()
	{	
		$data = array();
		$errors = array();
		$messages = "";
		try {		
			$template_id = $this->post('template_id');
			$customer_id = $this->post('customer_id');
			$send_type = $this->post('send_type');
			$active = $this->post('active');
			$send_day = $this->post('send_day');

			if(empty($template_id) || $template_id < 1)
				$errors[] = 'template_id';
			if(empty($send_type) || $send_day < 1)
				$errors[] = 'send_type';
			if(empty($customer_id) || $customer_id < 1)
				$errors[] = 'customer_id';
			$data['errors'] = $errors;		

			$cliente = $this->clientes->find($customer_id);

			if(count($errors) == 0) {
				$formData = $this->post();
				if(empty($active))
					$formData['active'] = "0";

				if(is_null($cliente)){
					$this->notFound();
				}else{

					$envio = $this->envios->findByClient($cliente->id);
					if(is_null($envio)){
						$resultId = $this->envios->add($formData);
						if($resultId > 0)
						{
							Dispatcher::redirectToAction('clientes','index');
						}
						else{
							$data['cliente'] = $cliente;
							$data['plantillas'] = $this->plantillas->getAll();
							$data['messages'] = "Error al crear el envio, favor verifique e intente nuevamente.";	
						}
					}else{
						Dispatcher::redirectToAction('envios','editar', array('cliente'=>$cliente->id));
					}
				}

			}else{
				if(is_null($cliente)){
					$this->notFound();
				}
				else{
					$data['cliente'] = $cliente;
					$data['plantillas'] = $this->plantillas->getAll();
					$data['messages'] = "Por favor verifique los campos obligatorios";
				}
			}
		} catch (Exception $e) {
			$data['messages'] = "El Envio no pudo ser creado por un error en la aplicación. "  . $e->getMessage();						
		}
		View::make('envios/crear', $data);
		
	}

	public function getEditar()
	{
		$data = array();
		try {
			if(is_null($this->queryString('cliente')) || $this->queryString('cliente') < 1){
				$this->notFound();
			}else{
				$cliente = $this->clientes->find($this->queryString('cliente'));				
				if(is_null($cliente))
				{
					View::make('error/notFound');
				}else{					
					$envio = $this->envios->findByClient($cliente->id);
					if(is_null($envio))
					{
						Dispatcher::redirectToAction('envios','crear', array('cliente'=>$cliente->id));
					}else
					{
						$data['cliente'] = $cliente;
						$data['envio'] = $envio;
						$data['plantillas'] = $this->plantillas->getAll();
						View::make('envios/editar', $data);
					}
				}	
			}						
		} catch (Exception $e) {
			$data['messages'] = "Error al intentar editar el envio. "  . $e->getMessage();						
			View::make('envios/editar', $data);
		}					
	}

	public function postEditar()
	{
		$data = array();
		$errors = array();
		$messages = "";
		try {		
			if(is_null($this->queryString('cliente')) || $this->queryString('cliente') < 1){
				$this->notFound();
			}else{
				$cliente = $this->clientes->find($this->queryString('cliente'));				
				if(is_null($cliente))
				{
					$this->notFound();
				}else{	
					$envio = $this->envios->findByClient($cliente->id);
					if(is_null($envio))
					{
						Dispatcher::redirectToAction('envios','crear', array('cliente'=>$cliente->id));
					}else
					{		
						$template_id = $this->post('template_id');
						$customer_id = $this->post('customer_id');
						$send_type = $this->post('send_type');						

						if(empty($template_id) || $template_id < 1)
							$errors[] = 'template_id';
						if(empty($send_type) || $send_type < 1)
							$errors[] = 'send_type';				
						if(empty($customer_id) || $customer_id < 1)
							$errors[] = 'customer_id';
						$data['errors'] = $errors;	

						if(count($errors) > 0 ) {
							$data['cliente'] = $cliente;
							$data['envio'] = $this->envios->findByClient($cliente->id);
							$data['plantillas'] = $this->plantillas->getAll();
							View::make('envios/editar', $data);						
						}
						else{

							$formData = $this->post();					
							$this->envios->edit($formData, $envio->id);							
							Dispatcher::redirectToAction('clientes','index'); 									
						}

					}

					
				}	
			}			
		} catch (Exception $e) {
			$data['messages'] = "El cliente no pudo ser modificado por un error en la aplicación. " . $e->getMessage();						
			View::make('clientes/editar', $data);
		}		
	}
}