<?php
class ErrorController extends BaseController
{
	protected $requireAuth = false;

	public function getNotFound()
	{
		header("HTTP/1.0 404 Not Found");
		View::make('error/notFound');
	}
}