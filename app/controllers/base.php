<?php
class BaseController
{
	protected $requireAuth = true;
	public $layout = null;

	public function __construct()
	{
		
	}

	public function requireAuth()
	{
		return $this->requireAuth;
	}

	public function notFound()
	{
		header("HTTP/1.0 404 Not Found");
		View::make('error/notFound'); 
	}

	public function applyAuth($action)
	{		
		if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    		// last request was more than 30 minutes ago
    		session_unset();     // unset $_SESSION variable for the run-time 
    		session_destroy();   // destroy session data in storage
		}

		$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp

		if(isset($_SESSION['auth']) && $_SESSION['auth']){
			$this->{$action}();			
		}else {
			Dispatcher::redirectToAction('auth','login'); 
		}	
	}

	protected function queryString($key = null)
	{
		if(is_null($key))
			return $_GET;
		
		if(array_key_exists($key, $_GET))		
			return $_GET[$key];		

		return null;
	}

	protected function post($key = null)
	{
		if(is_null($key))
			return $_POST;
		
		if(array_key_exists($key, $_POST))		
			return $_POST[$key];		

		return null;
	}

	protected function isPost()
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST')
			return true;
		return false;
	}

	public function useLayout()
	{
		if(!empty($this->layout))
			return true;
		return false;
	}

	public function getLayout()
	{
		return $this->layout;
	}

	public function userIsAuthenticated()
	{
		if(isset($_SESSION['auth']) && $_SESSION['auth'] == true)
			return true;
		
		return false;
	}
}