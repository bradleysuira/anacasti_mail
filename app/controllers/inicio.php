<?php

class InicioController extends BaseController
{	
	public $layout = 'layouts/default';

	public function getIndex()
	{
		View::make('inicio/index');
	}
}