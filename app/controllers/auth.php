<?php

class AuthController extends BaseController
{
	public $layout = 'layouts/login';
	protected $requireAuth = false;
	
	public function getIndex()
	{
		Dispatcher::redirectToAction('auth','login'); 
	}

	public function getLogin()
	{
		//cargamos la clase del modelo
		if($this->userIsAuthenticated())
			Dispatcher::redirectToAction('inicio','index'); 			

		View::make('auth/login', array('errorMessage'=>'',"postBack"=>false));	
	}

	public function postLogin()
	{	
		if($this->userIsAuthenticated())
				Dispatcher::redirectToAction('inicio','index'); 			

		Loader::resource('models', 'usuario');
		$users = new UsuarioModel();

		//obtenemos los valores del formulario
		$login = trim($this->post('user'));	
		$password = trim($this->post('password'));
		$message = "";
		$valid = true;

		//validamos el user
		if(is_null($login) || empty($login))
		{
			$message .= "debe ingresar su usuario";
			$valid = false;
		}

		//validamos el password
		if(is_null($password) || empty($password))
		{
			$message .= " debe ingresar su password";
			$valid = false;
		}

			if($valid) //sino hay errores
			{
				//autenticamos al usuario
				$user = $users->login($login, $password);

				if($user)
				{
					$this->auth($user);	
					
					Dispatcher::redirectToAction('inicio','index');
				}	
				else
					View::make('auth/login', array('errorMessage'=>"Usuario o contraseña no válida.","postBack"=>true));					
			}else{

				View::make('auth/login', array('errorMessage'=>"Debe ingresar su usuario y contraseña","postBack"=>true));
			}			
			
		}


		protected function auth($user)
		{
			$_SESSION['user'] = $user;
	        $_SESSION['auth'] = true;	
		}
		
		public function getLogout()
		{
			session_destroy();
			Dispatcher::redirectToAction('auth','login');
		}

	}