<?php
class PlantillasController extends BaseController
{	
	public $layout = 'layouts/default';
	protected $plantillas;

	public function __construct()
	{
		Loader::resource('models', 'plantilla');
		$this->plantillas = new PlantillaModel();

	}	public function getIndex()
	{
		
		$data = array();
		try {
			Loader::resource('lib', 'paginator');
			$count = $this->plantillas->count();
			$offSet = 10;
			$totalPages = ceil($count / $offSet);
			$page = ( is_null($this->queryString('page')) 
				|| ( !is_numeric($this->queryString('page')))
				|| ( $this->queryString('page') < 0 )
				|| ( $this->queryString('page') > $totalPages )
				) ? 1: $this->queryString('page');

			$limit = $offSet * ($page -1 );			
			$data['plantillas'] = $this->plantillas->getAll($limit, $offSet, "id desc");	
			
			$pages = Paginator::make($offSet , $page, $count);
			$data['paginator'] = $pages;			
		} catch (Exception $e) {
			$data['paginator'] = null;
			$data['messages'] = "Error al obtener la información de las plantillas . "  . $e->getMessage();
		}						

		View::make('plantillas/index', $data);							
	}

	public function getCrear()
	{	
		View::make('plantillas/crear');			
	}

	public function postCrear()
	{
		$data = array();
		$errors = array();
		$messages = "";
		try {		
			$name = $this->post('name');		
			$description = $this->post('description');
			$body = $this->post('body');

			if(empty($name))
				$errors[] = 'name';
			if(empty($description))
				$errors[] = 'description';
			if(empty($body))
				$errors[] = 'body';
			$data['errors'] = $errors;		

			if(count($errors) == 0) {
				$formData = $this->post();
				$resultId = $this->plantillas->add($formData);
				if($resultId > 0)
				{
					Dispatcher::redirectToAction('plantillas','index');
				}
				else{				
					$data['messages'] = "Error al crear la plantilla, favor verifique e intente nuevamente.";	
				}

			}else{
				$data['messages'] = "Por favor verifique los campos obligatorios";
			}
		} catch (Exception $e) {
			$data['messages'] = "La plantilla no pudo ser creada por un error en la aplicación. "  . $e->getMessage();						
		}
		View::make('plantillas/crear', $data);
	}

	public function getEditar()
	{
		$data = array();
		try {
			if(is_null($this->queryString('id')) || $this->queryString('id') < 1){
				$this->notFound();
			}else{
				$plantilla = $this->plantillas->find($this->queryString('id'));				
				if(is_null($plantilla))
				{
					$this->notFound();
				}else{											
					$data['plantilla'] = $plantilla;
					View::make('plantillas/editar', $data);
				}	
			}						
		} catch (Exception $e) {
			$data['messages'] = "Error al intentar editar la plnatilla. "  . $e->getMessage();						
			View::make('plantillas/editar', $data);
		}		
	}

	public function postEditar()
	{
		$data = array();
		$errors = array();
		$messages = "";
		try {		
			if(is_null($this->queryString('id')) || $this->queryString('id') < 1){
				$this->notFound();
			}else{
				$plantilla = $this->plantillas->find($this->queryString('id'));				
				if(is_null($plantilla))
				{
					$this->notFound();
				}else{		
					$name = $this->post('name');		
					$description = $this->post('description');
					$body = $this->post('body');

					if(empty($name))
						$errors[] = 'name';
					if(empty($description))
						$errors[] = 'description';
					if(empty($body))
						$errors[] = 'body';

					if(count($errors) > 0 ) {				
						$data['errors'] = $errors;	
						$data['plantilla'] = $plantilla;
						View::make('plantillas/editar', $data);						
					}
					else{				
						$formData = $this->post();					
						$this->plantillas->edit($formData, $this->queryString('id'));
						Dispatcher::redirectToAction('plantillas','index'); 									
					}													
				}	
			}
			
		} catch (Exception $e) {
			$data['messages'] = "La plantilla no pudo ser modificada por un error en la aplicación. " . $e->getMessage();						
			View::make('plantillas/editar', $data);
		}		
	}

	public function getEliminar()
	{
		$data = array();
		try {
			if(is_null($this->queryString('id')) || $this->queryString('id') < 1){
				$this->notFound();
			}else{
				$plantilla = $this->plantillas->find($this->queryString('id'));				
				if(is_null($plantilla))
				{
					$this->notFound();
				}	
				$data["plantilla"] = $plantilla;
				View::make('plantillas/eliminar', $data);
			}						
		} catch (Exception $e) {
			$data['messages'] = "Error al intentar eliminar la plantilla. "  . $e->getMessage();						
			View::make('plantillas/eliminar', $data);
		}		
	}

	public function postEliminar()
	{
		$data = array();				
		try {		
			if(is_null($this->queryString('id')) || $this->queryString('id') < 1){
				$this->notFound();
			}else{
				$plantilla = $this->plantillas->find($this->queryString('id'));				
				if(is_null($plantilla))
				{
					$this->notFound();
				}else{
					if($this->plantillas->count()> 1){							
						$this->plantillas->delete($this->queryString('id'));
						Dispatcher::redirectToAction('plantillas','index'); 
					}else{
						$data['messages'] = "No se pueden eliminar todas las plantillas, debe existir por lo menos una.";						
						View::make('plantillas/eliminar', $data);
					}
				}	
			}
			
		} catch (Exception $e) {
			$data['messages'] = "La plantilla no pudo ser eliminada por un error en la aplicación. " . $e->getMessage();						
			View::make('plantillas/eliminar', $data);
		}		
	}

	public function getPreliminar()
	{
		$this->layout = '';

		$data = array();				
		try {		
			if(is_null($this->queryString('id')) || $this->queryString('id') < 1){
				$this->notFound();
			}else{
				$plantilla = $this->plantillas->find($this->queryString('id'));				
				if(is_null($plantilla))
				{
					$this->notFound();
				}else{
					$data['plantilla'] = $plantilla->body;
					View::make('plantillas/preliminar', $data);		
				}	
			}
			
		} catch (Exception $e) {
			$this->layout = 'layouts/default';
			$data['messages'] = "La plantilla no pudo ser verificada . " . $e->getMessage();						
			View::make('plantillas/preliminar', $data);
		}

	}

}