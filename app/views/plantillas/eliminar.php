<h2 class="uk-article-title"> Eliminar Plantilla </h2>
<hr>
<div class="uk-grid-divider blank"></div>
<div class="uk-panel">
	<?php if(!empty($data->messages)): ?>
		<div class="uk-form-row">			
			<div class="uk-form-controls">
				<div class="uk-alert uk-alert-warning">
					<?php  echo $data->messages;	?>	
				</div> 					
				<a class="uk-button uk-button-large" type="button" data-uk-button href="javascript:history.back()"><i class="uk-icon-mail-reply"></i> Regresar</a>
			</div>
		</div>
	<?php else: 

	$plantilla = $data->plantilla;
	
	?>
	<form class="uk-form uk-form-horizontal" method="post" action="/?c=plantillas&a=eliminar&id=<?php echo $plantilla->id ?>">		
		<div class="uk-form-row">			
			<div class="uk-alert uk-alert-warning">
				¿ Está seguro de eliminar esta plantilla ?
			</div> 
		</div>
		<div class="uk-grid-divider"></div>	
		<div class="uk-form-row">
			<button class="uk-button uk-button-large uk-button-coop" type="submit"><i class="uk-icon-remove-sign"></i> Eliminar plantilla</button>
			<a class="uk-button uk-button-large" href="/?c=plantillas"><i class="uk-icon-mail-reply"></i> Cancelar</a>	
		</div>
	</form>
<?php endif; ?>
</div>
