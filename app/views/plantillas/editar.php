<h2 class="uk-article-title"> Editar Plantilla </h2>
<hr>
<div class="uk-grid-divider blank"></div>
<div class="uk-panel">
<?php if(!empty($data->messages)): ?>
			<div class="uk-form-row">			
				<div class="uk-form-controls">
					<div class="uk-alert uk-alert-warning">
					<?php  echo $data->messages;	?>	
					</div> 					
					<a class="uk-button uk-button-large" type="button" data-uk-button href="javascript:history.back()"><i class="uk-icon-mail-reply"></i> Regresar</a>
				</div>
			</div>
		<?php else: 

			$plantilla = $data->plantilla;
			
		?>
	<form class="uk-form uk-form-horizontal" method="post" action="/?c=plantillas&a=editar&id=<?php echo $plantilla->id ?>">		
		<div class="uk-form-row">
			<label class="uk-form-label" for="name">Nombre</label>
			<div class="uk-form-controls">
				<input type="text" id="name" name="name" class="uk-form-width-large <?php if(in_array('name', $data->errors)) echo 'uk-form-danger'; ?>" placeholder="Nombre" value="<?php echo $plantilla->name ?>">				
			</div>
		</div>
		<div class="uk-form-row">
			<label class="uk-form-label" for="notes">Descripcion</label>
			<div class="uk-form-controls">
				<textarea class="uk-form-width-large <?php if(in_array('description', $data->errors)) echo 'uk-form-danger'; ?>" cols="45" rows="2" name="description" id="description"  maxlength="250" placeholder="Descripción"><?php echo $plantilla->description ?></textarea>			
			</div>
		</div>	
		<div class="uk-form-row">
			<label class="uk-form-label" for="notes">Contenido</label>
			<div class="uk-form-controls">
				<textarea class="uk-form-width-large <?php if(in_array('body', $data->errors)) echo 'uk-form-danger'; ?>" cols="45" rows="20" name="body" id="body" placeholder="Contenido"><?php echo $plantilla->body ?></textarea>			
			</div>
		</div>	
		<div class="uk-form-row">				
				<a class="uk-button uk-button uk-button-warning uk-button-medium" href="#variables" data-uk-modal><i class="uk-icon-pushpin"></i> Variables</a>	
				<a class="uk-button uk-button uk-button-warning uk-button-medium" href="/?c=plantillas&a=preliminar&id=<?php echo $plantilla->id ?>" target="_blank" data-uk-modal><i class="uk-icon-zoom-in"></i> Vista preliminar</a>	
		</div>	
			<div id="variables" class="uk-modal">
				<div class="uk-modal-dialog">
					<a class="uk-modal-close uk-close"></a>
					<div class="uk-panel-box">
						<p>Variables disponibles para su uso en las plantillas de correo.</p>
						<ul class="uk-description-list">
							<li><strong>{{ruc}}</strong>: RUC</li>			
							<li><strong>{{name}}</strong>: Nombre del cliente</li>			
							<li><strong>{{last_pay}}</strong>: Último Pago</li>			
							<li><strong>{{last_pay_date}}</strong>: Fecha del último pago</li>			
							<li><strong>{{last_invoice}}</strong>: No. de la última factura</li>			
							<li><strong>{{last_invoice_date}}</strong>: Fecha de la última factura</li>			
							<li><strong>{{last_invoice_balance}}</strong>: Saldo de la última factura</li>			
							<li><strong>{{balance}}</strong>: Saldo actual</li>			
							<li><strong>{{expire_credit}}</strong>: Fecha de expiración del crédito</li>			
							<li><strong>{{credit_days}}</strong>: Días de crédito</li>								
						</ul>
					</div>
				</div>
			</div>	
		<div class="uk-grid-divider"></div>	
		<div class="uk-form-row">
			<button class="uk-button uk-button-large uk-button-coop" type="submit"><i class="uk-icon-save"></i> Guardar cambios</button>
			<a class="uk-button uk-button-large" href="/?c=plantillas"><i class="uk-icon-mail-reply"></i> Cancelar</a>	
		</div>
	</form>
	<?php endif; ?>
</div>
