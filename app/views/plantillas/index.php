<h2 class="uk-article-title"> Plantillas </h2>
<hr>
<div class="uk-grid-divider blank"></div>
<a class="uk-button uk-button-coop" data-uk-tooltip="{pos:'right'}" title="Crear nuevo cliente" href="/?c=plantillas&a=crear"><i class="uk-icon-plus-sign"></i> Nueva Plantilla</a>	
<table class="uk-table uk-table-hover uk-table-striped">
	<thead>
		<tr>
			<th>Nombre</th>
			<th class"hide-small">Descripcion</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data->plantillas as $plantilla) :?>
			<tr>
			<td><?php echo $plantilla->name ?></td>
			<td class"hide-small"><?php echo $plantilla->description ?></td>			
				<td>
					<div class="uk-button-group">						
						<a class="uk-button uk-button-invert" data-uk-tooltip="{pos:'top'}" title="Editar" href="/?c=plantillas&a=editar&id=<?php echo $plantilla->id ?>"><i class="uk-icon-edit"></i></a>													
						<a class="uk-button uk-button-danger" data-uk-tooltip="{pos:'top'}" title="Eliminar" href="/?c=plantillas&a=eliminar&id=<?php echo $plantilla->id ?>"><i class="uk-icon-remove-sign"></i></a>													
					</div>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php echo $data->paginator ?>