<div id="login-container" <?php if(!$data->postBack){ echo 'class="uk-animation-scale-up"';}?>>
	<div id="login-header">
		<img src="img/logo-anacasti.png">
	</div>
	<div id="login-content" class="clearfix">
		<div id="login" class="form-action show">
			<h1>Acceso a usuarios</h1>
			<p class="notice">Por favor introduzca su usuario y contraseña para ingresar al sistema.</p>
			<form method="post">
				<ul>
					<li>
						<input type="text" id="user" name="user" data-uk-tooltip="{pos:'right'}" title="Ingrese el usuario" placeholder="Usuario" required/>
					</li>
					<li>
						<input type="password" name="password" id="password" data-uk-tooltip="{pos:'right'}" title="Ingrese la contraseña" placeholder="Contraseña" required/>
					</li>
					<li>						
						<?php 
							if(!empty($data->errorMessage))
							{
								echo '<div class="uk-alert uk-alert-warning">'.
									 $data->errorMessage.
									 '</div>';
							}else{
								echo '<p></p>';
							}
						?>						
						<p>
							<button type="submit" class="uk-button uk-button-large uk-button-coop">Ingresar</button>
						</p>											
					</li>
			</ul>			
		</form>
	</div>  
</div>
</div>  
