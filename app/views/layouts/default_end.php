
</div>				
</div>   		
</div>

<div class="uk-grid-divider blank"></div>

</div>
<br>
<footer class="uk-text-center">
	<div class="uk-panel-box ">&copy;Anacasti - 2013</div>										
</footer>
<div id="menu-small" class="uk-offcanvas">
	<div class="uk-offcanvas-bar uk-offcanvas-bar-show">
		<ul class="uk-nav uk-nav-offcanvas">
			<li <?php if(View::$currentController == 'inicio' ) { echo 'class="uk-active"';}?>>
				<a href="/"><i class="uk-icon-home"></i> Inicio</a>
			</li>

			<li <?php if(View::$currentController == 'clientes' ) { echo 'class="uk-active"';}?>>
				<a href="/?c=clientes"><i class="uk-icon-group"></i> Clientes</a>
			</li>
			<li <?php if(View::$currentController == 'plantillas' ) { echo 'class="uk-active"';}?>>
				<a href="/?c=plantillas"><i class="uk-icon-file-text-alt"></i> Plantillas</a>
			</li>
			<li>
				<a href="/?c=auth&action=logout"><i class="uk-icon-signout"></i> Cerrar Sesión</a>						
			</li>				
		</ul>

	</div>

</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script> window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>
<script src="js/uikit.min.js"></script>
<script>
	$(function() {	
		$('.hide-small').hide();

		setTypeSend(parseInt($('#send_type').val()));	
		
		$('#send_type').change(function(){
			console.log($(this).val());
			$('#send_day_list').change(function(){
					setDayToSend($('#send_day_list').val());
				});
			$('#send_day_num').change(function(){
					setDayToSend($('#send_day_num').val());
				});
			var type = parseInt($(this).val());			
			setTypeSend(type);			
		});

		function setTypeSend(type)
		{
			switch(type)
			{
				case 1:	
				$('#row_send_day').hide(); 										
				setDayToSend(1);	
				$('#send_day_num').val(1);
				break;

				case 2:
				$('#row_send_day').show(); 			
				$('#send_day_num').hide();
				$('#send_day_list').show();				
				setDayToSend($('#send_day_list').val());	
				break;
			
				case 3:
				$('#row_send_day').show(); 			
				$('#send_day_num').show();
				$('#send_day_list').hide();
				setDayToSend($('#send_day_num').val());
				break;

				

				default:					
				$('#row_send_day').hide();			
				break;
			}
		}

		function setDayToSend(dayToSend)
		{
			$('#send_day').val(dayToSend);			
		}
	});

</script>
</body>
</html>

