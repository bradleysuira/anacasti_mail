<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Anacasti :: <?php echo $data->title ?></title>   
	<link rel="stylesheet" href="css/uikit.min.css">
	<link rel="stylesheet" href="css/site.css">
</head>
<body>			
	<nav class="uk-navbar uk-navbar-attached">		
		<div class="uk-container uk-container-center">				
			<div class="uk-navbar-brand uk-hidden-small" href="/"><img src="img/logo-anacasti.png" width="80" title="Anacasti" alt="Anacasti"></div>			
			<div class="uk-navbar-flip uk-hidden-small">
				<ul class="uk-navbar-nav">
					<li class="">
						<a href="/?c=auth&a=logout"><i class="uk-icon-signout"></i> Cerrar Sesión</a>
					</li>
				</ul>

			</div>	
		</div>
		<a href="#menu-small" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>		
		<div class="uk-navbar-brand uk-navbar-center uk-visible-small"><img src="img/logo-anacasti.png" width="80" title="Anacasti" alt="Anacasti"></div>		
	</nav>		
	<div class="uk-container uk-container-center uk-height-1-1" style="padding-bottom: 50px;">	
		<div class="uk-grid-divider blank"></div>

		<div class="uk-grid" data-uk-grid-match>	
			<div class="uk-width-medium-1-5">
				<div class="uk-panel uk-panel-box uk-hidden-small">
					<ul class="uk-nav uk-nav-side">
						<li class="uk-nav-divider"></li>
						<li <?php if(View::$currentController == 'inicio' ) { echo 'class="uk-active"';}?>>
							<a href="/"><i class="uk-icon-home"></i> Inicio</a>
						</li>
						<li <?php if(View::$currentController == 'clientes' || View::$currentController == 'envios' ) { echo 'class="uk-active"';}?>>
							<a href="/?c=clientes"><i class="uk-icon-group"></i> Clientes</a>
						</li>
						<li <?php if(View::$currentController == 'plantillas' ) { echo 'class="uk-active"';}?>>
							<a href="/?c=plantillas"><i class="uk-icon-file-text-alt"></i> Plantillas</a>
						</li>							
					</ul>
				</div>
			</div>		
			<div class="uk-width-4-5" uk-grid-margin>				
				<div class="uk-panel">				
					