
<?php if(!empty($data->messages)): ?>
	<h2 class="uk-article-title"> Envio </h2>
	<hr>
	<div class="uk-grid-divider blank"></div>
	<div class="uk-panel">
		<div class="uk-form-row">			
			<div class="uk-form-controls">
				<div class="uk-alert uk-alert-warning">
					<?php  echo $data->messages;	?>	
				</div> 					
				<a class="uk-button uk-button-warning" type="button" data-uk-button href="javascript:history.back()">Regresar</a>
			</div>
		</div>
	</div>			
<?php else: 
	$cliente = $data->cliente;
	?>
	<h2 class="uk-article-title"> Envio - <?php echo $cliente->name ?></h2>
	<hr>
	<div class="uk-grid-divider blank"></div>
	<div class="uk-panel">
		<form class="uk-form uk-form-horizontal" method="post" action="/?c=envios&a=crear&cliente=<?php echo $cliente->id ?>">		
			<div class="uk-form-row">
				<label class="uk-form-label" for="country_id">Plantilla </label>
				<div class="uk-form-controls">
					<select id="template_id" name="template_id" required>		
						<option value="">Seleccione...</option>			
						<?php 					
						foreach ($data->plantillas as $plantilla) {							
							echo '<option value="'.$plantilla->id .'">'. ucwords($plantilla->name).'</option>"';
						}
						?>
					</select>
				</div>
			</div>
			<div class="uk-form-row">
				<label class="uk-form-label" for="send_type">Frecuencia de Envío</label>
				<div class="uk-form-controls">
					<select id="send_type" name="send_type" required>		
						<option value="">Seleccione...</option>			
						<option value="1">Diario</option>			
						<option value="2">Semanal</option>			
						<option value="3">Mensual</option>			
					</select>
				</div>
			</div>	
			<div class="uk-form-row" id="row_send_day" style="display:none">
			<label class="uk-form-label" for="send_day">Día de envío</label>
				<div class="uk-form-controls">
					<input type="number" id="send_day_num" min="1" max="31" value="1" class="uk-form-width-mini uk-form-small">
					<select id="send_day_list">										
						<option value="1">Domingo</option>
						<option value="2">Lunes</option>			
						<option value="3">Martes</option>			
						<option value="4">Miércoles</option>			
						<option value="5">Jueves</option>			
						<option value="6">Viernes</option>			
						<option value="7">Sábado</option>												
					</select>
					<input type="hidden" name="send_day" id="send_day">
				</div>
			</div>	
			<div class="uk-form-row">
				<label class="uk-form-label" for="active">Estado del envío</label>
				<label><input type="radio" value="1" name="active"> Activo</label>
				<label><input type="radio" value="0" checked="checked" name="active"> Inactivo</label>
			</div>	
			<div class="uk-form-row">				
				<a class="uk-button uk-button uk-button-warning uk-button-medium" href="/?c=clientes&a=editar&id=<?php echo $cliente->id ?>"><i class="uk-icon-male"></i> Ver cliente</a>	
			</div>	
			<div class="uk-grid-divider"></div>	
			<div class="uk-form-row">
				<button class="uk-button uk-button-large uk-button-coop" type="submit"><i class="uk-icon-edit-sign"></i> Guardar Envio</button>
				<a class="uk-button uk-button-large" href="/?c=clientes"><i class="uk-icon-ban-circle"></i> Cancelar</a>	
			</div>
			<input type="hidden" name="customer_id" id="customer_id" value="<?php echo $cliente->id ?>">
		</form>
	<?php endif; ?>
</div>
