
<?php if(!empty($data->messages)): ?>
	<h2 class="uk-article-title"> Envio </h2>
	<hr>
	<div class="uk-grid-divider blank"></div>
	<div class="uk-panel">
		<div class="uk-form-row">			
			<div class="uk-form-controls">
				<div class="uk-alert uk-alert-warning">
					<?php  echo $data->messages;	?>	
				</div> 					
				<a class="uk-button uk-button-warning" type="button" data-uk-button href="javascript:history.back()">Regresar</a>
			</div>
		</div>
	</div>			
<?php else: 
	$cliente = $data->cliente;
	$envio = $data->envio;
	?>
	<h2 class="uk-article-title"> Envio - <?php echo $cliente->name ?></h2>
	<hr>
	<div class="uk-grid-divider blank"></div>
	<div class="uk-panel">
		<form class="uk-form uk-form-horizontal" method="post" action="/?c=envios&a=editar&cliente=<?php echo $cliente->id ?>">		
			<div class="uk-form-row">
				<label class="uk-form-label" for="template_id" >Plantilla </label>
				<div class="uk-form-controls">
					<select id="template_id" <?php if(in_array('template_id', $data->errors)) echo 'class="uk-form-danger"'; ?> name="template_id" required>		
						<option value="">Seleccione...</option>			
						<?php 					
						foreach ($data->plantillas as $plantilla) {	
							$selected = ($envio->template_id == $plantilla->id)	? "selected" : "";
							echo '<option value="'.$plantilla->id .'" '.$selected.'>'. ucwords($plantilla->name).'</option>"';													
						}
						?>
					</select>
				</div>
			</div>
			<div class="uk-form-row">
				<label class="uk-form-label" for="send_type" >Frecuencia de Envío</label>
				<div class="uk-form-controls">
					<select id="send_type" name="send_type" <?php if(in_array('send_type', $data->errors)) echo 'class="uk-form-danger"'; ?> required>		
						<option value="" <?php echo ($envio->send_type == 0 ?"selected":"")?>>Seleccione...</option>			
						<option value="1" <?php echo ($envio->send_type == 1 ?"selected":"")?>>Diario</option>			
						<option value="2" <?php echo ($envio->send_type == 2 ?"selected":"")?>>Semanal</option>									
						<option value="3" <?php echo ($envio->send_type == 3 ?"selected":"")?>>Mensual</option>			
					</select>
				</div>
			</div>	
			<div class="uk-form-row" id="row_send_day">
			<label class="uk-form-label" for="send_day">Día de envío</label>
				<div class="uk-form-controls">
					<input type="number" <?php echo ($envio->send_type < 4 ?"style='display:none'":"")?> id="send_day_num" min="1" max="31" value="<?php echo $envio->send_day ?>" class="uk-form-width-mini uk-form-small">
					<select id="send_day_list" <?php echo ($envio->send_type == 4 ?"style='display:none'":"")?> >										
						<option value="1" <?php echo ($envio->send_day == 1 ?"selected":"")?>>Domingo</option>			
						<option value="2" <?php echo ($envio->send_day == 2 ?"selected":"")?>>Lunes</option>			
						<option value="3" <?php echo ($envio->send_day == 3 ?"selected":"")?>>Martes</option>			
						<option value="4" <?php echo ($envio->send_day == 4 ?"selected":"")?>>Miércoles</option>			
						<option value="5" <?php echo ($envio->send_day == 5 ?"selected":"")?>>Jueves</option>			
						<option value="6" <?php echo ($envio->send_day == 6 ?"selected":"")?>>Viernes</option>			
						<option value="7" <?php echo ($envio->send_day == 7 ?"selected":"")?>>Sábado</option>			
					</select>
					<input type="hidden" name="send_day" id="send_day" value="<?php echo $envio->send_day ?>">
				</div>
			</div>	
			<div class="uk-form-row">
				<label class="uk-form-label" for="active">Estado del envío</label>
				<label><input type="radio" value="1" <?php echo $envio->active == 1 ?'checked="checked"':''; ?> name="active"> Activo</label>
				<label><input type="radio" value="0" <?php echo $envio->active != 1 ?'checked="checked"':''; ?> name="active"> Inactivo</label>
			</div>	
			<div class="uk-form-row">				
				<a class="uk-button uk-button uk-button-warning uk-button-medium" href="/?c=clientes&a=editar&id=<?php echo $cliente->id ?>"><i class="uk-icon-male"></i> Ver cliente</a>	
			</div>	
			<div class="uk-grid-divider"></div>	
			<div class="uk-form-row">
				<button class="uk-button uk-button-large uk-button-coop" type="submit"><i class="uk-icon-save"></i> Guardar Envio</button>
				<a class="uk-button uk-button-large" href="/?c=clientes"><i class="uk-icon-ban-circle"></i> Cancelar</a>	
			</div>
			<input type="hidden" name="customer_id" id="customer_id" value="<?php echo $cliente->id ?>">
		</form>
	<?php endif; ?>
</div>
