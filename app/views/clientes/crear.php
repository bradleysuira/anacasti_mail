<h2 class="uk-article-title"> Nuevo Cliente </h2>
<hr>
<div class="uk-grid-divider blank"></div>
<div class="uk-panel">
<?php if(!empty($data->messages)): ?>
			<div class="uk-form-row">			
				<div class="uk-form-controls">
					<div class="uk-alert uk-alert-warning">
					<?php  echo $data->messages;	?>	
					</div> 					
					<a class="uk-button uk-button-warning" type="button" data-uk-button href="javascript:history.back()">Regresar</a>
				</div>
			</div>
		<?php else: ?>
	<form class="uk-form uk-form-horizontal" method="post" action="/?c=clientes&a=crear">
		<div class="uk-form-row ">
			<label class="uk-form-label" for="ruc">RUC </label>
			<div class="uk-form-controls">
				<input type="text" class="uk-form-width-large <?php if(in_array('ruc', $data->errors)) echo 'uk-form-danger'; ?>" id="ruc" name="ruc" maxlength="45" placeholder="RUC" required>
			</div>
		</div>
		<div class="uk-form-row">
			<label class="uk-form-label" for="name">Nombre</label>
			<div class="uk-form-controls">
				<input type="text" class="uk-form-width-large <?php if(in_array('name', $data->errors)) echo 'uk-form-danger'; ?>" id="name" name="name" maxlength="250" placeholder="Nombre" required>
			</div>
		</div>
		<div class="uk-form-row">
			<label class="uk-form-label" for="email">Email</label>
			<div class="uk-form-controls">
				<input type="email" class="uk-form-width-large <?php if(in_array('email', $data->errors)) echo 'uk-form-danger'; ?>" id="email" name="email" maxlength="250" placeholder="Email" required>
			</div>
		</div>
		<div class="uk-form-row">
			<label class="uk-form-label" for="country_id">País</label>
			<div class="uk-form-controls">
				<select id="country_id" name="country_id" required>		
					<option value="">Seleccione...</option>			
					<?php 					
						foreach ($data->paises as $pais) {							
							echo '<option value="'.$pais->id .'">'. ucwords($pais->name).'</option>"';
						}
					?>
				</select>
			</div>
		</div>
		<div class="uk-form-row">
			<label class="uk-form-label" for="city">Ciudad</label>
			<div class="uk-form-controls">
				<input type="text" id="city" name="city" placeholder="Ciudad"  class="uk-form-width-large">				
			</div>
		</div>		
		<div class="uk-form-row">
			<label class="uk-form-label" for="last_invoice">Última factura</label>
			<div class="uk-form-controls">
				<input type="text" id="last_invoice" name="last_invoice" placeholder="Última factura">
				Fecha: <input type="date" id="last_invoice_date" name="last_invoice_date" placeholder="Fecha Última factura">
			</div>
		</div>
		<div class="uk-form-row">
			<label class="uk-form-label" for="last_pay">Último pago</label>
			<div class="uk-form-controls">
				<input type="text" id="last_pay" name="last_pay" maxlength="250" placeholder="Último pago">
				Fecha: <input type="date" id="last_pay_date" name="last_pay_date" maxlength="250" placeholder="Fecha último Pago">
			</div>
		</div>	
		<div class="uk-form-row">
			<label class="uk-form-label" for="last_invoice_balance">Balance última factura</label>
			<div class="uk-form-controls">
				<input type="text" id="last_invoice_balance" name="last_invoice_balance" placeholder="Balance última factura">				
			</div>
		</div>
		<div class="uk-form-row">
			<label class="uk-form-label" for="balance">Saldo actual</label>
			<div class="uk-form-controls">
				<input type="text" id="balance" name="balance" placeholder="Saldo actual">				
			</div>
		</div>
		<div class="uk-form-row">
			<label class="uk-form-label" for="expire_credit">Vencimiento del crédito</label>
			<div class="uk-form-controls">
				<input type="date" id="expire_credit" name="expire_credit" placeholder="Vencimiento del crédito">				
			</div>
		</div>
		<div class="uk-form-row">
			<label class="uk-form-label" for="credit_days">Días de crédito</label>
			<div class="uk-form-controls">
				<input type="text" id="credit_days" name="credit_days" placeholder="Días de crédito">				
			</div>
		</div>
		<div class="uk-form-row">
					<label class="uk-form-label" for="active">Estado </label>
				<label><input type="radio" value="1" checked="checked" name="active"> Activo</label>
				<label><input type="radio" value="0" name="active"> Inactivo</label>
		</div>
		<div class="uk-form-row">
			<label class="uk-form-label" for="notes">Observaciones</label>
			<div class="uk-form-controls">
				<textarea class="uk-form-width-large" cols="45" rows="4" name="notes" id="notes" maxlength="250" placeholder="Observaciones"></textarea>			
			</div>
		</div>		
		<div class="uk-grid-divider"></div>	
		<div class="uk-form-row">
			<button class="uk-button uk-button-large uk-button-coop" type="submit"><i class="uk-icon-plus-sign"></i> Crear cliente</button>
			<a class="uk-button uk-button-large" href="/?c=clientes"><i class="uk-icon-mail-reply"></i> Cancelar</a>	
		</div>
	</form>
	<?php endif; ?>
</div>
