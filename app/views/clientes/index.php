<h2 class="uk-article-title"> Clientes </h2>
<hr>
<div class="uk-grid-divider blank"></div>
<a class="uk-button uk-button-coop" data-uk-tooltip="{pos:'right'}" title="Crear nuevo cliente" href="/?c=clientes&a=crear"><i class="uk-icon-plus-sign"></i> Nuevo Cliente</a>	
<table class="uk-table uk-table-hover uk-table-striped">
	<thead>
		<tr>
			<th>Nombre</th>
			<th class"hide-small">Email</th>
			<th class"hide-small">Último Pago</th>
			<th class"hide-small">Saldo</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data->clientes as $cliente) :?>
			<tr>
			<td><?php echo $cliente->name ?></td>
			<td class"hide-small"><?php echo $cliente->email ?></td>
			<td class"hide-small" style="font-size: 65%"><?php echo date("m/d/Y g:i A", strtotime($cliente->last_pay_date))  ?></td>
			<td class"hide-small"><?php echo $cliente->balance ?></td>
				<td>
					<div class="uk-button-group">						
						<?php if($cliente->active == 1): ?>
							<a class="uk-button uk-button-invert" data-uk-tooltip="{pos:'top'}" title="Editar" href="/?c=clientes&a=editar&id=<?php echo $cliente->id ?>"><i class="uk-icon-edit"></i></a>							
							<a class="uk-button uk-button-invert" data-uk-tooltip="{pos:'top'}" title="Envios" href="/?c=envios&a=editar&cliente=<?php echo $cliente->id ?>"><i class="uk-icon-envelope"></i></a>
							<a class="uk-button uk-button-danger" data-uk-tooltip="{pos:'top'}" title="Eliminar" href="/?c=clientes&a=eliminar&id=<?php echo $cliente->id ?>"><i class="uk-icon-remove-sign"></i></a>																				
						<?php else: ?>
							<a class="uk-button uk-button-invert" data-uk-tooltip="{pos:'top'}" title="Editar - Cliente inactivo" href="/?c=clientes&a=editar&id=<?php echo $cliente->id ?>"><i class="uk-icon-edit"></i></a>							
							<a class="uk-button uk-button-danger" data-uk-tooltip="{pos:'top'}" title="Eliminar - Cliente inactivo" href="/?c=clientes&a=eliminar&id=<?php echo $cliente->id ?>"><i class="uk-icon-remove-sign"></i></a>													
						<?php endif; ?>
					</div>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php echo $data->paginator ?>