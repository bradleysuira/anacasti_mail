CREATE DATABASE  IF NOT EXISTS `anacasti_mail` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `anacasti_mail`;
-- MySQL dump 10.14  Distrib 5.5.32-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: anacasti_mail
-- ------------------------------------------------------
-- Server version	5.5.32-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(3) NOT NULL DEFAULT '',
  `name` char(52) NOT NULL DEFAULT '',
  `continent` enum('Asia','Europa','Norte América','Africa','Oceania','Antarctica','Sur América','América Centarl') NOT NULL DEFAULT 'América Centarl',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'ABW','Aruba',''),(2,'AFG','Afghanistan',''),(3,'AGO','Angola',''),(4,'AIA','Anguilla',''),(5,'ALB','Albania',''),(6,'AND','Andorra',''),(7,'ANT','Netherlands Antilles',''),(8,'ARE','United Arab Emirates',''),(9,'ARG','Argentina',''),(10,'ARM','Armenia',''),(11,'ASM','American Samoa',''),(12,'ATA','Antarctica',''),(13,'ATF','French Southern territories',''),(14,'ATG','Antigua and Barbuda',''),(15,'AUS','Australia',''),(16,'AUT','Austria',''),(17,'AZE','Azerbaijan',''),(18,'BDI','Burundi',''),(19,'BEL','Belgium',''),(20,'BEN','Benin',''),(21,'BFA','Burkina Faso',''),(22,'BGD','Bangladesh',''),(23,'BGR','Bulgaria',''),(24,'BHR','Bahrain',''),(25,'BHS','Bahamas',''),(26,'BIH','Bosnia and Herzegovina',''),(27,'BLR','Belarus',''),(28,'BLZ','Belize',''),(29,'BMU','Bermuda',''),(30,'BOL','Bolivia',''),(31,'BRA','Brasil',''),(32,'BRB','Barbados',''),(33,'BRN','Brunei',''),(34,'BTN','Bhutan',''),(35,'BVT','Bouvet Island',''),(36,'BWA','Botswana',''),(37,'CAF','Central African Republic',''),(38,'CAN','Canada',''),(39,'CCK','Cocos (Keeling) Islands',''),(40,'CHE','Switzerland',''),(41,'CHL','Chile',''),(42,'CHN','China',''),(43,'CIV','Côte d’Ivoire',''),(44,'CMR','Cameroon',''),(45,'COD','Congo, The Democratic Republic of the',''),(46,'COG','Congo',''),(47,'COK','Cook Islands',''),(48,'COL','Colombia',''),(49,'COM','Comoros',''),(50,'CPV','Cape Verde',''),(51,'CRI','Costa Rica',''),(52,'CUB','Cuba',''),(53,'CXR','Christmas Island',''),(54,'CYM','Cayman Islands',''),(55,'CYP','Cyprus',''),(56,'CZE','Czech Republic',''),(57,'DEU','Germany',''),(58,'DJI','Djibouti',''),(59,'DMA','Dominica',''),(60,'DNK','Denmark',''),(61,'DOM','Dominican Republic',''),(62,'DZA','Algeria',''),(63,'ECU','Ecuador',''),(64,'EGY','Egypt',''),(65,'ERI','Eritrea',''),(66,'ESH','Western Sahara',''),(67,'ESP','Spain',''),(68,'EST','Estonia',''),(69,'ETH','Ethiopia',''),(70,'FIN','Finland',''),(71,'FJI','Fiji Islands',''),(72,'FLK','Falkland Islands',''),(73,'FRA','France',''),(74,'FRO','Faroe Islands',''),(75,'FSM','Micronesia, Federated States of',''),(76,'GAB','Gabon',''),(77,'GBR','United Kingdom',''),(78,'GEO','Georgia',''),(79,'GHA','Ghana',''),(80,'GIB','Gibraltar',''),(81,'GIN','Guinea',''),(82,'GLP','Guadeloupe',''),(83,'GMB','Gambia',''),(84,'GNB','Guinea-Bissau',''),(85,'GNQ','Equatorial Guinea',''),(86,'GRC','Greece',''),(87,'GRD','Grenada',''),(88,'GRL','Greenland',''),(89,'GTM','Guatemala',''),(90,'GUF','French Guiana',''),(91,'GUM','Guam',''),(92,'GUY','Guyana',''),(93,'HKG','Hong Kong',''),(94,'HMD','Heard Island and McDonald Islands',''),(95,'HND','Honduras',''),(96,'HRV','Croatia',''),(97,'HTI','Haiti',''),(98,'HUN','Hungary',''),(99,'IDN','Indonesia',''),(100,'IND','India',''),(101,'IOT','British Indian Ocean Territory',''),(102,'IRL','Ireland',''),(103,'IRN','Iran',''),(104,'IRQ','Iraq',''),(105,'ISL','Iceland',''),(106,'ISR','Israel',''),(107,'ITA','Italy',''),(108,'JAM','Jamaica',''),(109,'JOR','Jordan',''),(110,'JPN','Japan',''),(111,'KAZ','Kazakstan',''),(112,'KEN','Kenya',''),(113,'KGZ','Kyrgyzstan',''),(114,'KHM','Cambodia',''),(115,'KIR','Kiribati',''),(116,'KNA','Saint Kitts and Nevis',''),(117,'KOR','South Korea',''),(118,'KWT','Kuwait',''),(119,'LAO','Laos',''),(120,'LBN','Lebanon',''),(121,'LBR','Liberia',''),(122,'LBY','Libyan Arab Jamahiriya',''),(123,'LCA','Saint Lucia',''),(124,'LIE','Liechtenstein',''),(125,'LKA','Sri Lanka',''),(126,'LSO','Lesotho',''),(127,'LTU','Lithuania',''),(128,'LUX','Luxembourg',''),(129,'LVA','Latvia',''),(130,'MAC','Macao',''),(131,'MAR','Morocco',''),(132,'MCO','Monaco',''),(133,'MDA','Moldova',''),(134,'MDG','Madagascar',''),(135,'MDV','Maldives',''),(136,'MEX','Mexico',''),(137,'MHL','Marshall Islands',''),(138,'MKD','Macedonia',''),(139,'MLI','Mali',''),(140,'MLT','Malta',''),(141,'MMR','Myanmar',''),(142,'MNG','Mongolia',''),(143,'MNP','Northern Mariana Islands',''),(144,'MOZ','Mozambique',''),(145,'MRT','Mauritania',''),(146,'MSR','Montserrat',''),(147,'MTQ','Martinique',''),(148,'MUS','Mauritius',''),(149,'MWI','Malawi',''),(150,'MYS','Malaysia',''),(151,'MYT','Mayotte',''),(152,'NAM','Namibia',''),(153,'NCL','New Caledonia',''),(154,'NER','Niger',''),(155,'NFK','Norfolk Island',''),(156,'NGA','Nigeria',''),(157,'NIC','Nicaragua',''),(158,'NIU','Niue',''),(159,'NLD','Netherlands',''),(160,'NOR','Norway',''),(161,'NPL','Nepal',''),(162,'NRU','Nauru',''),(163,'NZL','New Zealand',''),(164,'OMN','Oman',''),(165,'PAK','Pakistan',''),(166,'PAN','Panama',''),(167,'PCN','Pitcairn',''),(168,'PER','Peru',''),(169,'PHL','Philippines',''),(170,'PLW','Palau',''),(171,'PNG','Papua New Guinea',''),(172,'POL','Poland',''),(173,'PRI','Puerto Rico',''),(174,'PRK','North Korea',''),(175,'PRT','Portugal',''),(176,'PRY','Paraguay',''),(177,'PSE','Palestine',''),(178,'PYF','French Polynesia',''),(179,'QAT','Qatar',''),(180,'REU','Réunion',''),(181,'ROM','Romania',''),(182,'RUS','Russian Federation',''),(183,'RWA','Rwanda',''),(184,'SAU','Saudi Arabia',''),(185,'SDN','Sudan',''),(186,'SEN','Senegal',''),(187,'SGP','Singapore',''),(188,'SGS','South Georgia and the South Sandwich Islands',''),(189,'SHN','Saint Helena',''),(190,'SJM','Svalbard and Jan Mayen',''),(191,'SLB','Solomon Islands',''),(192,'SLE','Sierra Leone',''),(193,'SLV','El Salvador',''),(194,'SMR','San Marino',''),(195,'SOM','Somalia',''),(196,'SPM','Saint Pierre and Miquelon',''),(197,'STP','Sao Tome and Principe',''),(198,'SUR','Suriname',''),(199,'SVK','Slovakia',''),(200,'SVN','Slovenia',''),(201,'SWE','Sweden',''),(202,'SWZ','Swaziland',''),(203,'SYC','Seychelles',''),(204,'SYR','Syria',''),(205,'TCA','Turks and Caicos Islands',''),(206,'TCD','Chad',''),(207,'TGO','Togo',''),(208,'THA','Thailand',''),(209,'TJK','Tajikistan',''),(210,'TKL','Tokelau',''),(211,'TKM','Turkmenistan',''),(212,'TMP','East Timor',''),(213,'TON','Tonga',''),(214,'TTO','Trinidad and Tobago',''),(215,'TUN','Tunisia',''),(216,'TUR','Turkey',''),(217,'TUV','Tuvalu',''),(218,'TWN','Taiwan',''),(219,'TZA','Tanzania',''),(220,'UGA','Uganda',''),(221,'UKR','Ukraine',''),(222,'UMI','United States Minor Outlying Islands',''),(223,'URY','Uruguay',''),(224,'USA','United States',''),(225,'UZB','Uzbekistan',''),(226,'VAT','Holy See (Vatican cities State)',''),(227,'VCT','Saint Vincent and the Grenadines',''),(228,'VEN','Venezuela',''),(229,'VGB','Virgin Islands, British',''),(230,'VIR','Virgin Islands, U.S.',''),(231,'VNM','Vietnam',''),(232,'VUT','Vanuatu',''),(233,'WLF','Wallis and Futuna',''),(234,'WSM','Samoa',''),(235,'YEM','Yemen',''),(236,'YUG','Yugoslavia',''),(237,'ZAF','South Africa',''),(238,'ZMB','Zambia',''),(239,'ZWE','Zimbabwe',''),(242,'RDO','Rep. Dominicana','América Centarl');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ruc` varchar(45) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `country_id` int(11) NOT NULL,
  `last_pay` decimal(13,2) DEFAULT NULL,
  `last_pay_date` datetime DEFAULT NULL,
  `last_invoice` varchar(12) DEFAULT NULL,
  `last_invoice_date` datetime DEFAULT NULL,
  `last_invoice_balance` decimal(13,2) DEFAULT NULL,
  `balance` decimal(13,2) DEFAULT NULL,
  `expire_credit` datetime DEFAULT NULL,
  `credit_days` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `ruc_UNIQUE` (`ruc`),
  KEY `customers_ibfk_1` (`country_id`),
  CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,NULL,'Humberto Garcia/Camilo Vargas','hugarciag@hotmail.com',48,50000.00,'2013-05-21 00:00:00','0393/13','2013-05-29 00:00:00',30825.00,90565.00,'2013-08-29 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Bogota'),(2,NULL,'Edgar Garcia/Pablo Forero','reg1995@hotmail.com',48,10806.52,'2013-02-20 00:00:00','2628/13','2013-03-04 00:00:00',30475.48,30475.48,'2013-06-04 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Bogota'),(3,NULL,'Victor Alvarez/Dist. A y D ltda','distriayd@yahoo.com',48,50027.95,'2013-05-27 00:00:00','0130/13','2013-02-22 00:00:00',36.05,36.05,'2013-05-22 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Bogota'),(4,NULL,'Jarbey Rodriguez','jrimportaciones@hotmail.com',48,10500.00,'2012-02-28 00:00:00','0191/12','2012-03-08 00:00:00',60155.00,60678.00,'2012-06-08 00:00:00',90,1,'jrimportaciones1@hotmail.com; ','2013-09-16 05:25:41',NULL,'Bogota'),(5,NULL,'Juan Nieto','distribuidora.juni@hotmail.com',48,500000.00,'2013-06-10 00:00:00','0375/13','2013-05-15 00:00:00',120792.00,7090540.30,'2013-12-15 00:00:00',210,1,'; ','2013-09-16 05:25:41',NULL,'Bogota'),(6,NULL,'Alberto Nieto','nietoimportaciones@yahoo.com',48,100000.00,'2013-05-14 00:00:00','0283/13','2013-04-11 00:00:00',90557.00,1600655.64,'2013-08-11 00:00:00',120,1,'albertonietoimportaciones@gmail.com; nietoimportaciones@hotmail.com','2013-09-16 05:25:41',NULL,'Bogota'),(7,NULL,'Osorio Melba','0.168399003491177230.4061359748301',48,10008.00,'2013-02-18 00:00:00','0238/13','2013-03-21 00:00:00',768.00,868.00,'2013-06-21 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Cali'),(8,NULL,'Rosero Herman','0.51078116010015940.87454393587498',48,70000.00,'2013-02-19 00:00:00','0134/13','2013-02-22 00:00:00',20367.00,110158.00,'2013-05-22 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Cali'),(9,NULL,'Abelardo Gomez/Wilson Gomez','importadoratoys@hotmail.com',48,50100.00,'2013-08-21 00:00:00','0217/12','2012-03-14 00:00:00',20194.00,20194.00,'2012-06-14 00:00:00',90,1,'imporgoar@hotmail.com; ','2013-09-16 05:25:41',NULL,'Cali'),(10,NULL,'Luis Fernando Garcia Salazar','sebesg@gmail.com',48,250631.27,'2013-04-09 00:00:00','0240/13','2013-03-26 00:00:00',390129.20,6440112.13,'2013-12-26 00:00:00',270,1,'comprasebesg@gmail.com; ','2013-09-16 05:25:41',NULL,'Cali'),(11,NULL,'Dario Sanchez','dariodivandi@hotmail.com',48,80000.00,'2013-01-31 00:00:00','0898/12','2012-09-20 00:00:00',973.91,973.91,'2012-12-20 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Cali'),(12,NULL,'Meneses Isabel','isamene@hotmail.com',48,0.00,'0000-00-00 00:00:00','0133/13','2013-02-22 00:00:00',30959.00,120054.00,'2013-05-22 00:00:00',90,1,'beatry99@hotmail.com; ','2013-09-16 05:25:41',NULL,'Barranquilla'),(13,NULL,'Carlos Raul  Garcia','mercamaravillas@hotmail.com',48,0.00,'0000-00-00 00:00:00','0165/13','2013-03-04 00:00:00',160531.00,160531.00,'2013-06-04 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Barrancabermeja'),(14,NULL,'Olarte Pastor','viveroartesaniasbosque@yahoo.com',48,0.00,'0000-00-00 00:00:00','0186/13','2013-03-06 00:00:00',320494.00,320494.00,'2013-06-06 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Bucaramanga'),(15,NULL,'Bienes Santa Marta/Otilia Avellaneda','yoti323@hotmail.com',48,10145.00,'2013-03-05 00:00:00','0170/13','2013-03-05 00:00:00',20233.00,20233.00,'2013-06-05 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Barranquilla'),(16,NULL,'C.I Flores de Liz S.A','electorivera@yahoo.com',48,0.00,'0000-00-00 00:00:00','0211/13','2013-03-13 00:00:00',80992.00,80992.00,'2013-06-13 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Barranquilla'),(17,NULL,'Hernandez Hector','sandrajulietteean@hotmail.com',48,30017.00,'2013-02-19 00:00:00','0115/13','2013-02-20 00:00:00',10000.00,10000.00,'2013-05-20 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'San Andres'),(18,NULL,'Leon Jairo','jairprimave@hotmail.com',48,10692.00,'2011-12-13 00:00:00','1222/13','2011-12-13 00:00:00',10300.00,10300.00,'2012-03-13 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Cartagena'),(19,NULL,'La Cascada','variedadesplasdecor@yahoo.es',48,90600.00,'2011-05-13 00:00:00','35136/11','2011-05-12 00:00:00',10008.00,10008.00,'2011-08-12 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Monteria'),(20,NULL,'Hector Garcia/Camilo Peñaranda','hectorgarcia_motivos@hotmail.com',48,0.00,'0000-00-00 00:00:00','0213/13','2013-03-13 00:00:00',60658.00,60658.00,'2013-06-13 00:00:00',90,1,'motivos@telecom.com.co; ','2013-09-16 05:25:41',NULL,'Cucuta'),(21,NULL,'Omaira Pavon/ Dubia Piedraita','anitaleyvapabon@hotmail.com',48,70000.00,'2012-04-04 00:00:00','0353/12','2012-04-25 00:00:00',80898.00,80898.00,'2012-07-25 00:00:00',90,1,'loreleyva83@hotmail.com; ','2013-09-16 05:25:41',NULL,'Cucuta'),(22,NULL,'Ligia Barrera','ligiaytu@hotmail.com',48,140885.00,'2009-03-27 00:00:00','0334/09','2009-04-03 00:00:00',250809.00,250809.00,'2009-07-03 00:00:00',90,1,'blancaligiaba@hotmail.com; ','2013-09-16 05:25:41',NULL,'Cucuta'),(23,NULL,'Luis Rodriguez','luisrodriguezjimenez@hotmail.com',48,200000.00,'2013-05-28 00:00:00','1105/12','2012-10-29 00:00:00',280242.50,860551.77,'0000-00-00 00:00:00',120,1,'; ','2013-09-16 05:25:41',NULL,'Cucuta'),(24,NULL,'Alberto Ramirez','variedades@caroleste.com.co',48,10404.60,'2013-05-09 00:00:00','0255/13','2013-04-04 00:00:00',390280.89,7650901.89,'2013-12-04 00:00:00',240,1,'; ','2013-09-16 05:25:41',NULL,'Medellin'),(25,NULL,'Claudia Osorio/Amparo Salazar','0.048708820760910230.1543117856181',48,10000.00,'2013-05-08 00:00:00','0825/12','2012-09-07 00:00:00',10278.00,40658.00,'2012-12-07 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Medellin'),(26,NULL,'Gloria Mejia/Fransisco Aguirre','gloriamego@hotmail.com',48,140600.00,'2013-04-22 00:00:00','0233/13','2013-03-20 00:00:00',170582.00,170582.00,'2013-06-20 00:00:00',90,1,'montraly@yahoo.es; ','2013-09-16 05:25:41',NULL,'Medellin'),(27,NULL,'Castro Enelda','carlosmariosc@hotmail.com',48,70000.00,'2013-04-17 00:00:00','0263/13','2013-04-05 00:00:00',30105.00,30339.00,'2013-07-05 00:00:00',90,1,'enelda26@gmail.com; ','2013-09-16 05:25:41',NULL,'Medellin'),(28,NULL,'Edwin Giraldo/Ricardo Ramirez','tomysamy@hotmail.com',48,0.00,'0000-00-00 00:00:00','0245/13','2013-03-26 00:00:00',120130.00,120130.00,'2013-06-26 00:00:00',90,1,'d.multimundo@yahoo.com; ','2013-09-16 05:25:41',NULL,'Medellin'),(29,NULL,'Jonathan Hernandez/Carmen Yaneth','0.71120065423771790.14792715119936',48,50000.00,'2012-01-08 00:00:00','1460/07','2007-10-04 00:00:00',70662.00,70662.00,'2008-01-04 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Medellin'),(30,NULL,'Luis Angel Valencia','0.40987683963950430.27670042987605',48,30584.00,'2012-05-18 00:00:00','0153/12','2012-02-24 00:00:00',69.00,69.00,'2012-05-24 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Medellin'),(31,NULL,'Julio Jaramillo/Dairo Zuluaga','dairozuluaga@live.com',48,0.00,'0000-00-00 00:00:00','0090/13','2013-02-07 00:00:00',10835.00,10835.00,'2013-05-07 00:00:00',90,1,'yerojuli@hotmail.com; ','2013-09-16 05:25:41',NULL,'Medellin'),(32,NULL,'Victor Gomez','victorgomez827@hotmail.com',48,700836.62,'2013-05-16 00:00:00','0102/13','2013-02-15 00:00:00',130407.00,3470383.91,'2013-12-15 00:00:00',300,1,'; ','2013-09-16 05:25:41',NULL,'Medellin'),(33,NULL,'Magic Garden','magicgarden82@gmail.com',48,100000.00,'2012-11-15 00:00:00','0219/13','2013-03-14 00:00:00',80283.00,320278.05,'2013-06-14 00:00:00',90,1,'magicgarden82@hotmail.com; nuevojardin2006@hotmail.com','2013-09-16 05:25:41',NULL,'Maicao'),(34,NULL,'Almacen Monalisa Sport','0.91578226621801240.93972072651583',48,40000.00,'2013-01-31 00:00:00','1291/12','2012-12-17 00:00:00',40105.00,40105.00,'2013-03-17 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Maicao'),(35,NULL,'Almacen El Boliche','0.349280842905194340.8685023736846',48,500.00,'2012-09-13 00:00:00','0860/12','2012-09-13 00:00:00',130900.00,190556.00,'2012-12-13 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Maicao'),(36,NULL,'Almacen Campo Florido','campo.florido@hotmail.com',48,100920.00,'2013-05-31 00:00:00','0237/13','2013-03-21 00:00:00',350610.00,370969.56,'2012-12-17 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Maicao'),(37,NULL,'Atif Hay/ Almacen El Boliche','0.99905744660557940.52334971960945',48,20000.00,'2010-10-07 00:00:00','1336/10','2010-10-22 00:00:00',70860.00,70860.00,'2011-01-22 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Barranquilla'),(38,NULL,'Comercial Zarif Toys','yasser_hachem@hotmail.com',48,30867.00,'2013-04-26 00:00:00','0365/13','2013-05-09 00:00:00',530310.00,530977.00,'2013-12-09 00:00:00',210,1,'zarif.colombia@hotmail.com; zarif.maicao@gmail.com','2013-09-16 05:25:41',NULL,'Maicao'),(39,NULL,'Almacen Gran Nasa','elgrannasa@hotmail.com',48,1090946.28,NULL,'',NULL,0.00,-1070548.86,NULL,90,1,'; ','2013-09-16 05:25:41',NULL,'Maicao'),(40,NULL,'Almacen Corozal','liliaesther45@hotmail.com',48,490941.95,'2013-06-03 00:00:00','0191/13','2013-03-07 00:00:00',210630.00,640772.71,'2013-12-07 00:00:00',270,1,'almacencorozal@hotmail.com; ','2013-09-16 05:25:41',NULL,'Maicao'),(41,NULL,'Daxiang Ltda/ZI Chan L.Q Ltda','daxianglin@hotmail.com',51,300743.00,'2013-06-25 00:00:00','0390/13','2013-05-27 00:00:00',130656.00,340250.55,'2013-08-27 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'San José'),(42,NULL,'JB de San José','jdjim_r@hotmail.com',51,0.00,'0000-00-00 00:00:00','0328/13','2013-04-23 00:00:00',10574.00,10574.00,'2013-07-23 00:00:00',90,1,'lesbiarr@hotmail.com; ','2013-09-16 05:25:41',NULL,'San José'),(43,NULL,'Eduardo Cartin y CIA','eduardo3@ice.co.cr',51,0.00,'0000-00-00 00:00:00','0946/12','2012-09-27 00:00:00',336.00,336.00,'2012-12-27 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'San José'),(44,NULL,'Almacen Corella','ccorella@racsa.co.cr',51,0.00,'0000-00-00 00:00:00','0369/13','2013-05-13 00:00:00',10773.00,10773.00,'2013-08-13 00:00:00',90,1,'ccorella@sol.racsa.co.cr; ','2013-09-16 05:25:41',NULL,'San José'),(45,NULL,'Luma S.A','luma_sa@racsa.co.cr',51,90900.00,'2013-05-13 00:00:00','0260/13','2013-04-04 00:00:00',161.00,161.00,'2013-07-04 00:00:00',90,1,'lumasa@ice.co.cr; impdonjose@ice.co.cr','2013-09-16 05:25:41',NULL,'San José'),(46,NULL,'Inv.Comercial El Punto Perfecto','rosmyquiros@hotmail.com',51,40177.00,'2013-05-13 00:00:00','0108/13','2013-02-19 00:00:00',40.00,40.00,'2013-05-19 00:00:00',90,1,'jmolinal79@hotmail.com; mgarrom@racsa.co.cr','2013-09-16 05:25:41',NULL,'San José'),(47,NULL,'Samar Internacional S.A/Oscar Arias','osarias01@hotmail.com',51,10500.00,'2009-04-16 00:00:00','1245/08','2008-10-08 00:00:00',120969.00,360942.00,'2009-01-08 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'San José'),(48,NULL,'JJSM Distribuciones SRL','alejimos@racsa.co.cr',51,240000.00,'2013-05-22 00:00:00','0959/12','2012-10-01 00:00:00',330355.62,1360937.86,'2013-07-01 00:00:00',270,1,'; ','2013-09-16 05:25:41',NULL,'San José'),(49,NULL,'Rene Madrid','fenix-rema@hotmail.com',157,0.00,'0000-00-00 00:00:00','0189/13','2013-03-06 00:00:00',50644.00,50644.00,'2013-06-06 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(50,NULL,'Reina Ramirez','dbozad@gmail.com',157,60000.00,'2013-05-20 00:00:00','0257/13','2013-04-03 00:00:00',60991.00,60991.00,'2013-07-03 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(51,NULL,'Juan Carlos Garcia Vivas','jdvirus2002@hotmail.com',157,0.00,'0000-00-00 00:00:00','0182/13','2013-03-06 00:00:00',100058.00,100058.00,'2013-06-06 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(52,NULL,'Eric Ugarte','aalcocer9@yahoo.com',157,0.00,'0000-00-00 00:00:00','0319/13','2013-04-18 00:00:00',30784.00,30784.00,'2013-07-18 00:00:00',90,1,'casabonita1@turbonett.com.ni; alv.15_01_89@hotmail.com','2013-09-16 05:25:41',NULL,''),(53,NULL,'Guillermo Toledo','0.94744473504595890.01124150772694',157,20500.00,'2013-04-24 00:00:00','0336/13','2013-04-26 00:00:00',20808.00,90170.00,'2013-07-26 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,''),(54,NULL,'Edgar Vivas Escobar','nicablock@yahoo.com',157,40000.00,'2013-05-17 00:00:00','0187/13','2013-03-06 00:00:00',10881.00,10881.00,'2013-06-06 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(55,NULL,'Wendy Mejia','wendymejianc@hotmail.com',157,0.00,'0000-00-00 00:00:00','0358/13','2013-05-02 00:00:00',20594.00,20594.00,'2013-08-02 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(56,NULL,'Juana Arana','0.74005136614670180.48615841053999',157,20000.00,'2013-06-05 00:00:00','0355/13','2013-05-02 00:00:00',50867.00,60867.00,'2013-08-02 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,''),(57,NULL,'Aristides Orozco','conejito_bello10@yahoo.es',157,0.00,'0000-00-00 00:00:00','0343/13','2013-04-30 00:00:00',50334.00,50334.00,'2013-07-30 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(58,NULL,'Alizaga Dominga','josealizaga20@hotmail.com',157,40976.00,'0000-00-00 00:00:00','0313/13','2013-04-16 00:00:00',30721.00,30737.00,'2013-07-16 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(59,NULL,'Blanca Ramirez/Rogerio Fonseca','tiendablanquitaryb2011@hotmail.com',157,70300.00,'2013-03-21 00:00:00','0327/13','2013-04-22 00:00:00',20822.00,20822.00,'2013-07-22 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(60,NULL,'Janeth Vivas Escobar','0.85792265632927670.39706756025279',157,0.00,'0000-00-00 00:00:00','0397/13','2013-05-29 00:00:00',30309.00,30309.00,'2013-08-29 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,''),(61,NULL,'Deyanira Valdivia Baez','deyaniravaldivia@gmail.com',157,0.00,'0000-00-00 00:00:00','0226/13','2013-03-19 00:00:00',70072.00,70072.00,'2013-06-19 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(62,NULL,'Pedro Rafael Medrano','0.069459213939923060.5268626003776',157,0.00,'0000-00-00 00:00:00','0163/13','2013-03-01 00:00:00',30235.00,30235.00,'2013-06-01 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,''),(63,NULL,'Alex Garcia Vivas Escobar','0.77352813144543030.44311035186341',157,200.00,'2012-11-06 00:00:00','1213/09','2009-10-15 00:00:00',869.00,869.00,'2010-01-15 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,''),(64,NULL,'Maria Cifuentes','pronein@hotmail.com',63,0.00,'0000-00-00 00:00:00','0266/13','2013-04-05 00:00:00',150078.00,150078.00,'2013-07-05 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(65,NULL,'With Love S.A','0.65926304614102750.63496398891784',63,30945.95,'2013-04-01 00:00:00','0230/13','2013-03-19 00:00:00',60376.05,60376.05,'2013-06-19 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Guayaquil'),(66,NULL,'Claudio Contreras','claudio0562@hotmail.com',63,0.00,'0000-00-00 00:00:00','0321/13','2013-04-19 00:00:00',20151.00,20151.00,'2013-07-19 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Cuencas'),(67,NULL,'Yesenia Ordoñez','0.97573086710249160.84548891973261',95,0.00,'0000-00-00 00:00:00','0293/13','2013-04-11 00:00:00',20893.00,20893.00,'2013-07-11 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,''),(68,NULL,'Adolfo Garcia','fran_mayde@hotmail.com',95,200.00,'2013-04-10 00:00:00','0823/08','2008-07-29 00:00:00',60903.00,60903.00,'2008-10-29 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(69,NULL,'Casa Junier/Distribuidora Z','casajunier@gmail.com',166,0.00,'0000-00-00 00:00:00','0076/13','2013-02-04 00:00:00',40164.00,40164.00,'2013-05-04 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(70,NULL,'Ofideusa Chitre S.A','0.90086522782302010.32255266264318',166,200.00,'2013-03-25 00:00:00','1187/10','2010-10-06 00:00:00',560.44,560.44,'2011-01-06 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Chitre'),(71,NULL,'Distribuidora Rogmar S.A','rogermojica@hotmail.es',166,30500.00,'2013-05-29 00:00:00','0394/13','2013-05-29 00:00:00',60772.00,170281.00,'2013-08-29 00:00:00',90,1,'rogmarpanama@hotmail.com; rogmar@cwpanama.net','2013-09-16 05:25:41',NULL,'Panamá'),(72,NULL,'Distribuidora UPIM','0.57713356854127120.07629658475173',166,0.00,'0000-00-00 00:00:00','0386/13','2013-05-23 00:00:00',70290.00,70290.00,'2013-08-23 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Panamá'),(73,NULL,'Distribuidora Kakey','0.183072189970940520.4138249665627',166,0.00,'0000-00-00 00:00:00','0368/13','2013-05-10 00:00:00',50635.00,50635.00,'2013-08-10 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,''),(74,NULL,'Junta Comunal El Pajaro','jlcorrea13@hotmail.com',166,0.00,'0000-00-00 00:00:00','1194/12','2012-10-29 00:00:00',60255.08,60255.08,'2013-01-29 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,'Pese'),(75,NULL,'Checherito Del Hogar','0.183960274964534020.8402351092921',166,50.00,'2013-04-22 00:00:00','1173/11','2011-11-17 00:00:00',300.00,300.00,'2012-02-17 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Colón'),(76,NULL,'Distribuidora Rafael','0.37058483564349340.95970067750634',166,20159.00,'2013-03-01 00:00:00','0012/13','2013-01-14 00:00:00',-50.00,-50.00,'2013-04-14 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,''),(77,NULL,'Era Zona Libre/Inv. Monami','0.30104338405751010.27779809038881',166,20000.00,'2012-11-01 00:00:00','1358/10','2010-10-27 00:00:00',90434.00,680014.00,'2011-01-27 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Zona Libre'),(78,NULL,'Super Ventas Chitre','0.393462444090715070.5098884501586',166,10482.00,'2009-11-30 00:00:00','1182/09','2009-10-12 00:00:00',10557.00,10557.00,'2010-01-12 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,'Chitre'),(79,NULL,'Murano C.A','alexander680@hotmail.com',228,10000.00,'2013-05-22 00:00:00','0387/13','2013-05-24 00:00:00',30066.00,80124.00,'2013-08-24 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(80,NULL,'Servi Import Export C.A','servimportiii@gmail.com',228,150000.00,'2013-03-20 00:00:00','0908/12','2012-09-21 00:00:00',510421.00,610017.00,'2012-12-21 00:00:00',90,1,'servi.import.export@gmail.com; ','2013-09-16 05:25:41',NULL,''),(81,NULL,'Girasol Moussa C.A','hamethabdelwa@hotmail.com',228,0.00,'0000-00-00 00:00:00','1220/12','2012-11-22 00:00:00',150983.00,150983.00,'2013-02-22 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(82,NULL,'Comercial Hos. Farah C.A/Imp.Assia Yossef','chf1988@gmail.com',228,50947.90,'2013-03-25 00:00:00','1135/12','2012-10-31 00:00:00',52.10,52.10,'2013-01-31 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(83,NULL,'La Bomba','planetasalha@hotmail.com',228,40980.41,'2013-02-04 00:00:00','0646/12','2012-08-02 00:00:00',20512.59,20512.59,'2012-11-02 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(84,NULL,'Distribuidora Deby C.A','babycolordevenezuela@hotmail.com',228,364.00,'2010-01-29 00:00:00','0978/09','2009-09-11 00:00:00',12.00,12.00,'2009-12-11 00:00:00',90,1,'distribuidoradeby@cantv.net; ','2013-09-16 05:25:41',NULL,''),(85,NULL,'Mazal Import/Leo Benlolo','leobenlolo@gmail.com',228,0.00,'0000-00-00 00:00:00','0067/13','2013-01-31 00:00:00',30284.00,30284.00,'0000-00-00 00:00:00',90,1,'jonathanbenlolo@yahoo.com; ','2013-09-16 05:25:41',NULL,''),(86,NULL,'Jozel Import/Lady Import Export','jozelimport1@gmail.com',168,300063.95,'2013-05-08 00:00:00','0366/13','2013-05-09 00:00:00',740541.87,3090473.44,'2013-12-09 00:00:00',210,1,'; ','2013-09-16 05:25:41',NULL,''),(87,NULL,'Importadora Sidon','almirsamad@impsidon.com.br',31,30000.00,'2013-05-28 00:00:00','0180/13','2013-03-05 00:00:00',60531.00,60531.00,'2013-06-05 00:00:00',90,1,'impsidon@osite.com.br; ','2013-09-16 05:25:41',NULL,''),(88,NULL,'Manuel Melo','jessi_melo12@hotmail.com',242,0.00,'0000-00-00 00:00:00','0307/13','2013-04-15 00:00:00',80918.00,80918.00,'2013-07-15 00:00:00',90,1,'; ','2013-09-16 05:25:41',NULL,''),(89,NULL,'Importadora Ley','carlos.posadas@yahoo.com',89,0.00,'0000-00-00 00:00:00','0261/13','2013-04-05 00:00:00',40582.00,40582.00,'2013-07-05 00:00:00',90,1,'leyimp@hotmail.com; ','2013-09-16 05:25:41',NULL,''),(90,NULL,'Nueva Casa Mercedita/Julio Tejada','0.064182099014690240.7160480103604',242,200.00,'2010-05-31 00:00:00','0415/08','2008-04-04 00:00:00',20847.00,20847.00,'2008-07-04 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,''),(91,NULL,'Plaza Indira/Pedro Abreu','0.14052319353495090.05057473206014',242,10000.00,'2011-11-23 00:00:00','0910/08','2008-08-19 00:00:00',140800.00,140800.00,'2008-11-19 00:00:00',90,0,'; ','2013-09-16 05:25:41',NULL,''),(128,'1234-568-565656','prueba','bradley@gideapps.com',1,56.00,'2012-11-30 00:00:00','25','2008-01-14 00:00:00',25.00,550.35,'2009-11-25 00:00:00',90,1,'','2013-09-16 06:55:47',NULL,'Panamá');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailing`
--

DROP TABLE IF EXISTS `mailing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `send_type` int(11) NOT NULL DEFAULT '1',
  `send_day` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `template_id` (`template_id`),
  CONSTRAINT `mailing_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `mailing_ibfk_2` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailing`
--

LOCK TABLES `mailing` WRITE;
/*!40000 ALTER TABLE `mailing` DISABLE KEYS */;
INSERT INTO `mailing` VALUES (1,1,4,1,0,0),(2,89,4,1,1,0),(3,128,4,3,16,1);
/*!40000 ALTER TABLE `mailing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates`
--

LOCK TABLES `templates` WRITE;
/*!40000 ALTER TABLE `templates` DISABLE KEYS */;
INSERT INTO `templates` VALUES (4,'Basic Template','Plantilla básica','<htm>\r\n<head>\r\n<title>Anacasti Notifications</title>\r\n</head>\r\n<body>\r\n<p>\r\nEstimado cliente: <strong>{{name}}</strong>\r\n</p>\r\n<p>\r\nSu cuenta presenta actualmente un saldo de $ <strong>{{balance}}</strong>, correspondiente a la factura No. {{last_invoice}}, de {{last_invoice_date}}\r\n</p>\r\n<p>\r\nAtentamente,\r\n</p>\r\n<p>\r\n<img src=\"http://anacastimail.dev/img/logo-anacasti.png\">\r\n</p>\r\n</body>\r\n</htm>');
/*!40000 ALTER TABLE `templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Administrator','Mailing','mailAdmin','admin@anacasti.com','2c4b34c35a4d7de6daa90312e6185e340db285ae');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-09-16  2:08:42
