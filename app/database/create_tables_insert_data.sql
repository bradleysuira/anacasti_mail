USE `anacasti_mail`;

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,	
	login VARCHAR(255) NOT NULL UNIQUE,
	email VARCHAR(255) NOT NULL UNIQUE,
	password VARCHAR(45) NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE countries(
  id INT NOT NULL AUTO_INCREMENT,
  code char(3) NOT NULL DEFAULT '' UNIQUE,
  name char(52) NOT NULL DEFAULT '',
  local_name char(45) NOT NULL DEFAULT '',
  continent enum('Asia','Europa','Norte America','Africa','Oceania','Antarctica','Sur America', 'America Centarl') NOT NULL DEFAULT 'America Centarl',
  PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE templates(
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(250) NULL,
  body text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE customers(
	id INT NOT NULL AUTO_INCREMENT,	
	ruc VARCHAR(45) NULL UNIQUE,
	name VARCHAR(255) NOT NULL UNIQUE,
	email VARCHAR(255) NOT NULL UNIQUE,
	country_id INT NOT NULL,
	city VARCHAR(255) NULL,
	last_pay DECIMAL(13, 2) NULL,
	last_pay_date DATETIME NULL,
	last_invoice INT NULL,
	last_invoice_date DATETIME NULL,
	last_invoice_balance DECIMAL(13, 2) NULL,
	balance DECIMAL(13, 2) NULL,
	expire_credit DATETIME NULL,
	credit_days INT NULL,
	notes VARCHAR(500) NULL,
	active INT NULL DEFAULT 1,
	created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  
  	updated datetime DEFAULT NULL, 
	PRIMARY KEY (`id`),	
	FOREIGN KEY (country_id) REFERENCES countries(id)	
)ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE mailing(
	id INT NOT NULL AUTO_INCREMENT,
	customer_id INT NOT NULL,	
	template_id INT NOT NULL,
	send_type INT NOT NULL DEFAULT 1, -- diario:1; semanal:2; quincenal:3; mensual:4
	send_day INT NOT NULL, -- diario:0 /  semanal, quincenal: {1-7 dia semana} / mensual: {1-31 dia mes}
	active INT NOT NULL,
	PRIMARY KEY (`id`),	
	FOREIGN KEY (customer_id) REFERENCES customers(id),	
	FOREIGN KEY (template_id) REFERENCES templates(id)	
);



-- INSERTS
-- password sha1: anacasti_Admin 
INSERT INTO users (first_name,
				   last_name,	
			       login,
				   email,
				   password)
			VALUES('Administrator',
				   'Mailing',
				   'mailAdmin',
				   'admin@anacasti.com',
				   '2c4b34c35a4d7de6daa90312e6185e340db285ae');




LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'ABW','Aruba','North America','Aruba'),(2,'AFG','Afghanistan','Asia','Afganistan/Afqanestan'),(3,'AGO','Angola','Africa','Angola'),(4,'AIA','Anguilla','North America','Anguilla'),(5,'ALB','Albania','Europe','Shqipëria'),(6,'AND','Andorra','Europe','Andorra'),(7,'ANT','Netherlands Antilles','North America','Nederlandse Antillen'),(8,'ARE','United Arab Emirates','Asia','Al-Imarat al-´Arabiya al-Muttahida'),(9,'ARG','Argentina','South America','Argentina'),(10,'ARM','Armenia','Asia','Hajastan'),(11,'ASM','American Samoa','Oceania','Amerika Samoa'),(12,'ATA','Antarctica','Antarctica','–'),(13,'ATF','French Southern territories','Antarctica','Terres australes françaises'),(14,'ATG','Antigua and Barbuda','North America','Antigua and Barbuda'),(15,'AUS','Australia','Oceania','Australia'),(16,'AUT','Austria','Europe','Österreich'),(17,'AZE','Azerbaijan','Asia','Azärbaycan'),(18,'BDI','Burundi','Africa','Burundi/Uburundi'),(19,'BEL','Belgium','Europe','België/Belgique'),(20,'BEN','Benin','Africa','Bénin'),(21,'BFA','Burkina Faso','Africa','Burkina Faso'),(22,'BGD','Bangladesh','Asia','Bangladesh'),(23,'BGR','Bulgaria','Europe','Balgarija'),(24,'BHR','Bahrain','Asia','Al-Bahrayn'),(25,'BHS','Bahamas','North America','The Bahamas'),(26,'BIH','Bosnia and Herzegovina','Europe','Bosna i Hercegovina'),(27,'BLR','Belarus','Europe','Belarus'),(28,'BLZ','Belize','North America','Belize'),(29,'BMU','Bermuda','North America','Bermuda'),(30,'BOL','Bolivia','South America','Bolivia'),(31,'BRA','Brasil','South America','Brasil'),(32,'BRB','Barbados','North America','Barbados'),(33,'BRN','Brunei','Asia','Brunei Darussalam'),(34,'BTN','Bhutan','Asia','Druk-Yul'),(35,'BVT','Bouvet Island','Antarctica','Bouvetøya'),(36,'BWA','Botswana','Africa','Botswana'),(37,'CAF','Central African Republic','Africa','Centrafrique/Bê-Afrîka'),(38,'CAN','Canada','North America','Canada'),(39,'CCK','Cocos (Keeling) Islands','Oceania','Cocos (Keeling) Islands'),(40,'CHE','Switzerland','Europe','Schweiz/Suisse/Svizzera/Svizra'),(41,'CHL','Chile','South America','Chile'),(42,'CHN','China','Asia','Zhongquo'),(43,'CIV','Côte d’Ivoire','Africa','Côte d’Ivoire'),(44,'CMR','Cameroon','Africa','Cameroun/Cameroon'),(45,'COD','Congo, The Democratic Republic of the','Africa','République Démocratique du Congo'),(46,'COG','Congo','Africa','Congo'),(47,'COK','Cook Islands','Oceania','The Cook Islands'),(48,'COL','Colombia','South America','Colombia'),(49,'COM','Comoros','Africa','Komori/Comores'),(50,'CPV','Cape Verde','Africa','Cabo Verde'),(51,'CRI','Costa Rica','North America','Costa Rica'),(52,'CUB','Cuba','North America','Cuba'),(53,'CXR','Christmas Island','Oceania','Christmas Island'),(54,'CYM','Cayman Islands','North America','Cayman Islands'),(55,'CYP','Cyprus','Asia','Kýpros/Kibris'),(56,'CZE','Czech Republic','Europe','¸esko'),(57,'DEU','Germany','Europe','Deutschland'),(58,'DJI','Djibouti','Africa','Djibouti/Jibuti'),(59,'DMA','Dominica','North America','Dominica'),(60,'DNK','Denmark','Europe','Danmark'),(61,'DOM','Dominican Republic','North America','República Dominicana'),(62,'DZA','Algeria','Africa','Al-Jaza’ir/Algérie'),(63,'ECU','Ecuador','South America','Ecuador'),(64,'EGY','Egypt','Africa','Misr'),(65,'ERI','Eritrea','Africa','Ertra'),(66,'ESH','Western Sahara','Africa','As-Sahrawiya'),(67,'ESP','Spain','Europe','España'),(68,'EST','Estonia','Europe','Eesti'),(69,'ETH','Ethiopia','Africa','YeItyop´iya'),(70,'FIN','Finland','Europe','Suomi'),(71,'FJI','Fiji Islands','Oceania','Fiji Islands'),(72,'FLK','Falkland Islands','South America','Falkland Islands'),(73,'FRA','France','Europe','France'),(74,'FRO','Faroe Islands','Europe','Føroyar'),(75,'FSM','Micronesia, Federated States of','Oceania','Micronesia'),(76,'GAB','Gabon','Africa','Le Gabon'),(77,'GBR','United Kingdom','Europe','United Kingdom'),(78,'GEO','Georgia','Asia','Sakartvelo'),(79,'GHA','Ghana','Africa','Ghana'),(80,'GIB','Gibraltar','Europe','Gibraltar'),(81,'GIN','Guinea','Africa','Guinée'),(82,'GLP','Guadeloupe','North America','Guadeloupe'),(83,'GMB','Gambia','Africa','The Gambia'),(84,'GNB','Guinea-Bissau','Africa','Guiné-Bissau'),(85,'GNQ','Equatorial Guinea','Africa','Guinea Ecuatorial'),(86,'GRC','Greece','Europe','Elláda'),(87,'GRD','Grenada','North America','Grenada'),(88,'GRL','Greenland','North America','Kalaallit Nunaat/Grønland'),(89,'GTM','Guatemala','North America','Guatemala'),(90,'GUF','French Guiana','South America','Guyane française'),(91,'GUM','Guam','Oceania','Guam'),(92,'GUY','Guyana','South America','Guyana'),(93,'HKG','Hong Kong','Asia','Xianggang/Hong Kong'),(94,'HMD','Heard Island and McDonald Islands','Antarctica','Heard and McDonald Islands'),(95,'HND','Honduras','North America','Honduras'),(96,'HRV','Croatia','Europe','Hrvatska'),(97,'HTI','Haiti','North America','Haïti/Dayti'),(98,'HUN','Hungary','Europe','Magyarország'),(99,'IDN','Indonesia','Asia','Indonesia'),(100,'IND','India','Asia','Bharat/India'),(101,'IOT','British Indian Ocean Territory','Africa','British Indian Ocean Territory'),(102,'IRL','Ireland','Europe','Ireland/Éire'),(103,'IRN','Iran','Asia','Iran'),(104,'IRQ','Iraq','Asia','Al-´Iraq'),(105,'ISL','Iceland','Europe','Ísland'),(106,'ISR','Israel','Asia','Yisra’el/Isra’il'),(107,'ITA','Italy','Europe','Italia'),(108,'JAM','Jamaica','North America','Jamaica'),(109,'JOR','Jordan','Asia','Al-Urdunn'),(110,'JPN','Japan','Asia','Nihon/Nippon'),(111,'KAZ','Kazakstan','Asia','Qazaqstan'),(112,'KEN','Kenya','Africa','Kenya'),(113,'KGZ','Kyrgyzstan','Asia','Kyrgyzstan'),(114,'KHM','Cambodia','Asia','Kâmpuchéa'),(115,'KIR','Kiribati','Oceania','Kiribati'),(116,'KNA','Saint Kitts and Nevis','North America','Saint Kitts and Nevis'),(117,'KOR','South Korea','Asia','Taehan Min’guk (Namhan)'),(118,'KWT','Kuwait','Asia','Al-Kuwayt'),(119,'LAO','Laos','Asia','Lao'),(120,'LBN','Lebanon','Asia','Lubnan'),(121,'LBR','Liberia','Africa','Liberia'),(122,'LBY','Libyan Arab Jamahiriya','Africa','Libiya'),(123,'LCA','Saint Lucia','North America','Saint Lucia'),(124,'LIE','Liechtenstein','Europe','Liechtenstein'),(125,'LKA','Sri Lanka','Asia','Sri Lanka/Ilankai'),(126,'LSO','Lesotho','Africa','Lesotho'),(127,'LTU','Lithuania','Europe','Lietuva'),(128,'LUX','Luxembourg','Europe','Luxembourg/Lëtzebuerg'),(129,'LVA','Latvia','Europe','Latvija'),(130,'MAC','Macao','Asia','Macau/Aomen'),(131,'MAR','Morocco','Africa','Al-Maghrib'),(132,'MCO','Monaco','Europe','Monaco'),(133,'MDA','Moldova','Europe','Moldova'),(134,'MDG','Madagascar','Africa','Madagasikara/Madagascar'),(135,'MDV','Maldives','Asia','Dhivehi Raajje/Maldives'),(136,'MEX','Mexico','North America','México'),(137,'MHL','Marshall Islands','Oceania','Marshall Islands/Majol'),(138,'MKD','Macedonia','Europe','Makedonija'),(139,'MLI','Mali','Africa','Mali'),(140,'MLT','Malta','Europe','Malta'),(141,'MMR','Myanmar','Asia','Myanma Pye'),(142,'MNG','Mongolia','Asia','Mongol Uls'),(143,'MNP','Northern Mariana Islands','Oceania','Northern Mariana Islands'),(144,'MOZ','Mozambique','Africa','Moçambique'),(145,'MRT','Mauritania','Africa','Muritaniya/Mauritanie'),(146,'MSR','Montserrat','North America','Montserrat'),(147,'MTQ','Martinique','North America','Martinique'),(148,'MUS','Mauritius','Africa','Mauritius'),(149,'MWI','Malawi','Africa','Malawi'),(150,'MYS','Malaysia','Asia','Malaysia'),(151,'MYT','Mayotte','Africa','Mayotte'),(152,'NAM','Namibia','Africa','Namibia'),(153,'NCL','New Caledonia','Oceania','Nouvelle-Calédonie'),(154,'NER','Niger','Africa','Niger'),(155,'NFK','Norfolk Island','Oceania','Norfolk Island'),(156,'NGA','Nigeria','Africa','Nigeria'),(157,'NIC','Nicaragua','North America','Nicaragua'),(158,'NIU','Niue','Oceania','Niue'),(159,'NLD','Netherlands','Europe','Nederland'),(160,'NOR','Norway','Europe','Norge'),(161,'NPL','Nepal','Asia','Nepal'),(162,'NRU','Nauru','Oceania','Naoero/Nauru'),(163,'NZL','New Zealand','Oceania','New Zealand/Aotearoa'),(164,'OMN','Oman','Asia','´Uman'),(165,'PAK','Pakistan','Asia','Pakistan'),(166,'PAN','Panama','North America','Panamá'),(167,'PCN','Pitcairn','Oceania','Pitcairn'),(168,'PER','Peru','South America','Perú/Piruw'),(169,'PHL','Philippines','Asia','Pilipinas'),(170,'PLW','Palau','Oceania','Belau/Palau'),(171,'PNG','Papua New Guinea','Oceania','Papua New Guinea/Papua Niugini'),(172,'POL','Poland','Europe','Polska'),(173,'PRI','Puerto Rico','North America','Puerto Rico'),(174,'PRK','North Korea','Asia','Choson Minjujuui In´min Konghwaguk (Bukhan)'),(175,'PRT','Portugal','Europe','Portugal'),(176,'PRY','Paraguay','South America','Paraguay'),(177,'PSE','Palestine','Asia','Filastin'),(178,'PYF','French Polynesia','Oceania','Polynésie française'),(179,'QAT','Qatar','Asia','Qatar'),(180,'REU','Réunion','Africa','Réunion'),(181,'ROM','Romania','Europe','România'),(182,'RUS','Russian Federation','Europe','Rossija'),(183,'RWA','Rwanda','Africa','Rwanda/Urwanda'),(184,'SAU','Saudi Arabia','Asia','Al-´Arabiya as-Sa´udiya'),(185,'SDN','Sudan','Africa','As-Sudan'),(186,'SEN','Senegal','Africa','Sénégal/Sounougal'),(187,'SGP','Singapore','Asia','Singapore/Singapura/Xinjiapo/Singapur'),(188,'SGS','South Georgia and the South Sandwich Islands','Antarctica','South Georgia and the South Sandwich Islands'),(189,'SHN','Saint Helena','Africa','Saint Helena'),(190,'SJM','Svalbard and Jan Mayen','Europe','Svalbard og Jan Mayen'),(191,'SLB','Solomon Islands','Oceania','Solomon Islands'),(192,'SLE','Sierra Leone','Africa','Sierra Leone'),(193,'SLV','El Salvador','North America','El Salvador'),(194,'SMR','San Marino','Europe','San Marino'),(195,'SOM','Somalia','Africa','Soomaaliya'),(196,'SPM','Saint Pierre and Miquelon','North America','Saint-Pierre-et-Miquelon'),(197,'STP','Sao Tome and Principe','Africa','São Tomé e Príncipe'),(198,'SUR','Suriname','South America','Suriname'),(199,'SVK','Slovakia','Europe','Slovensko'),(200,'SVN','Slovenia','Europe','Slovenija'),(201,'SWE','Sweden','Europe','Sverige'),(202,'SWZ','Swaziland','Africa','kaNgwane'),(203,'SYC','Seychelles','Africa','Sesel/Seychelles'),(204,'SYR','Syria','Asia','Suriya'),(205,'TCA','Turks and Caicos Islands','North America','The Turks and Caicos Islands'),(206,'TCD','Chad','Africa','Tchad/Tshad'),(207,'TGO','Togo','Africa','Togo'),(208,'THA','Thailand','Asia','Prathet Thai'),(209,'TJK','Tajikistan','Asia','Toçikiston'),(210,'TKL','Tokelau','Oceania','Tokelau'),(211,'TKM','Turkmenistan','Asia','Türkmenostan'),(212,'TMP','East Timor','Asia','Timor Timur'),(213,'TON','Tonga','Oceania','Tonga'),(214,'TTO','Trinidad and Tobago','North America','Trinidad and Tobago'),(215,'TUN','Tunisia','Africa','Tunis/Tunisie'),(216,'TUR','Turkey','Asia','Türkiye'),(217,'TUV','Tuvalu','Oceania','Tuvalu'),(218,'TWN','Taiwan','Asia','T’ai-wan'),(219,'TZA','Tanzania','Africa','Tanzania'),(220,'UGA','Uganda','Africa','Uganda'),(221,'UKR','Ukraine','Europe','Ukrajina'),(222,'UMI','United States Minor Outlying Islands','Oceania','United States Minor Outlying Islands'),(223,'URY','Uruguay','South America','Uruguay'),(224,'USA','United States','North America','United States'),(225,'UZB','Uzbekistan','Asia','Uzbekiston'),(226,'VAT','Holy See (Vatican cities State)','Europe','Santa Sede/Città del Vaticano'),(227,'VCT','Saint Vincent and the Grenadines','North America','Saint Vincent and the Grenadines'),(228,'VEN','Venezuela','South America','Venezuela'),(229,'VGB','Virgin Islands, British','North America','British Virgin Islands'),(230,'VIR','Virgin Islands, U.S.','North America','Virgin Islands of the United States'),(231,'VNM','Vietnam','Asia','Viêt Nam'),(232,'VUT','Vanuatu','Oceania','Vanuatu'),(233,'WLF','Wallis and Futuna','Oceania','Wallis-et-Futuna'),(234,'WSM','Samoa','Oceania','Samoa'),(235,'YEM','Yemen','Asia','Al-Yaman'),(236,'YUG','Yugoslavia','Europe','Jugoslavija'),(237,'ZAF','South Africa','Africa','South Africa'),(238,'ZMB','Zambia','Africa','Zambia'),(239,'ZWE','Zimbabwe','Africa','Zimbabwe'),(239,'RDO','Rep. Dominicana','America','America Central');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

