<?php
class EnvioModel extends BaseModel
{
	protected $table = "mailing";

	public function findByClient($id)
	{
		$query = "select * from " . $this->table . " where customer_id = :id";
		$values = array("id" => $id);
		$result = DB::query($query, $values);
		if(count($result) > 0)
			return $result[0];
		return null;
	}

	public function findByTemplate($id)
	{
		$query = "select * from " . $this->table . " where template_id = :id";
		$values = array("id" => $id);
		$result = DB::query($query, $values);
		if(count($result) > 0)
			return $result[0];
		return null;
	}
}