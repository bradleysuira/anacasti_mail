<?php
class ClienteModel extends BaseModel
{
	protected $table = "customers";

	public function getClientsByStatus($status = 1)
	{
		$query = "select * from " 
		. $this->table 
		. " where active = :active";
		
		return DB::query($query,array('active'=>$status));;
	}

	public function getMailingData()
	{
		$query = "select  c.ruc, 
				  	c.name, 
				  	c.email, 
					c.last_pay, 
					c.last_pay_date, 
					c.last_invoice, 
					c.last_invoice_date,
					c.last_invoice_balance, 
					c.balance, 
					c.expire_credit, 
					c.credit_days,
					m.send_type,
					m.send_day,
					m.template_id,	
					t.body as template_body
					from customers c
					join mailing m
					on m.customer_id = c.id
					join templates t
					on m.template_id = t.id
					where m.active = 1
					and c.active = 1
					and (
					 (
						m.send_type = 1 
					 )
					 or (
						m.send_type = 2
					    and DAYOFWEEK(CURDATE()) = m.send_day
					 )
					 or (
						m.send_type = 3						
						and (DAYOFMONTH(CURDATE()) = m.send_day
							 or (
									LAST_DAY(CURDATE()) < 29
									and m.send_day in(29,30,31)
					                and DAYOFMONTH(CURDATE()) = 28
								)
						    )
					 )
					)";
		
		return DB::query($query);
	}
}