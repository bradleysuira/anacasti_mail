<?php 
class BaseModel
{	
	protected $table;

	public function find($id)
	{
		$query = "select * from " . $this->table . " where id = :id";
		$values = array("id" => $id);
		$result = DB::query($query, $values);
		if(count($result) > 0)
			return $result[0];
		return null;
	}

	public function count()
	{
		$query = "select count(*) from " . $this->table;
		$values = array();
		
		return DB::count($query, $values);
	}

	public function paginate($query = null)
	{
		
	}
	
	public function getAll($limit = null, $offSet = 10, $order = "")
	{
		$query = "select * from " 
			     . $this->table ;	

		if(!empty($order))		     
		{
			$query .= " order by " . $order;
		}
		
		if(!is_null($limit))		     
		{
			$query .= " limit " . $limit . ", " .$offSet;			
		}		

		return DB::query($query);		
	}

	public function getAllAsArray()
	{
		$query = "select * from " 
			     . $this->table ;			     
		
		return DB::query($query, array(), PDO::FETCH_ASSOC);		
	}

	public function add($model = array())
	{
		$query = "insert into " . $this->table . " (#keys#) values(#values#)";
		$keys = "";
		$values = "";	
		$data = array();	
		
		foreach ($model as $key => $value) {
			$keys .= $key . ",";
			$values .= " :".$key .",";
			$data[$key] = $value;
		}
		
		$values = substr($values, 0, -1);
		$keys = substr($keys, 0, -1);
		$query = str_replace("#keys#", $keys, $query);
		$query = str_replace("#values#", $values, $query);

		return DB::insert($query, $data);
	}

	public function edit($model = array(), $id)
	{
		$query = "update " . $this->table . " set ";		
		$data = array();	
		
		foreach ($model as $key => $value) {
			if($key != "id")
			{
				$query .= $key . ' = :' . $key . ", ";				
			}
			$data[$key] = $value;
		}

		$query = substr($query, 0, -2);
		
		$query .= " where id = :id" ;
		$data['id'] = $id;		

		return DB::update($query, $data);

	}

	public function delete($id)
	{
		$query = "delete from " . $this->table 				
			   . " where id = :id" ;

		return DB::delete($query,array('id'=>$id));

	}
}
