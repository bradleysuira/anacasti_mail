<?php
class UsuarioModel extends BaseModel
{
	protected $table = "users";

	public function login($login, $password)
	{
		$query = "select id, login, first_name, last_name, email from " 
			     . $this->table . " where login = :login and password = :password";
			     
		$values = array(
					"login" => trim($login),
					"password" => sha1(trim($password))
				  );		
		return DB::query($query, $values);		
	}
}