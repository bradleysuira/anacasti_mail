<?php
date_default_timezone_set('America/Panama');
define('BASE_PATH', dirname(__FILE__) . '/');

//start session if not active
if (session_status() !== PHP_SESSION_ACTIVE) {session_start();}

//include required files
require_once 'lib/config.php';
require_once 'lib/loader.php';
require_once 'lib/database.php';
require_once 'models/base.php';
include 'lib/mailer.php';


//setup config
Config::run();

//load required resources
Loader::resource('models', 'cliente');
Loader::resource('models', 'plantilla');

//get mail values
$domain = Config::get('mail::domain');
$apiKey = Config::get('mail::apiKey');

$mailer = new Mailer($domain, $apiKey);
$clientes = new ClienteModel();

//get Clients to send
$dataClientes = $clientes->getMailingData();


foreach ($dataClientes as $cliente) {
	//replace variables in boy
	if(!is_null($cliente->email) 
		&& !empty($cliente->email) 
		&& strpos($cliente->email,'@') !== false){
		$body = trim($cliente->template_body);
		$body = str_replace("{{ruc}}", $cliente->ruc, $body);
		$body = str_replace("{{name}}", $cliente->name, $body);
		$body = str_replace("{{last_pay}}", date("m/d/Y", strtotime($cliente->last_pay)), $body);
		$body = str_replace("{{last_pay_date}}", date("m/d/Y", strtotime($cliente->last_pay_date)), $body);
		$body = str_replace("{{last_invoice}}", $cliente->last_invoice, $body);
		$body = str_replace("{{last_invoice_date}}", date("m/d/Y", strtotime($cliente->last_invoice_date)), $body);
		$body = str_replace("{{last_invoice_balance}}", $cliente->last_invoice_balance, $body);
		$body = str_replace("{{balance}}", $cliente->balance, $body);
		$body = str_replace("{{expire_credit}}", date("m/d/Y", strtotime($cliente->expire_credit)), $body);
		$body = str_replace("{{credit_days}}", $cliente->credit_days, $body);

		$mailer->send($body,"notifications",trim($cliente->email));
	}
}

echo "OK";



