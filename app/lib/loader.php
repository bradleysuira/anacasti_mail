<?php
class Loader
{
	public static function resource($path, $resource)
	{
		$fileName = BASE_PATH . $path .'/'. $resource . '.php';

		if(!file_exists($fileName))
			throw new Exception("Error Loading resource : ". $fileName, 1);
		
		require_once $fileName;			
		
	}
}
