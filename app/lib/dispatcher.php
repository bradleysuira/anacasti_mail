<?php
class Dispatcher
{
	protected static $controllerName;
	protected static $actionName;
	protected static $controller;
	protected static $fileName;
	protected static $controllerPath;

	private function __construct(){}

	public static function run() 
	{  
		try {
				self::$controllerPath = "inicio";
				self::$actionName = "index";
				$dispatch = null;				

				if(isset($_GET['c']) && !(empty($_GET['c'])))
					self::$controllerPath = strtolower(trim($_GET['c']));

				if(isset($_GET['a']) && !(empty($_GET['a'])))
					self::$actionName = strtolower(trim($_GET['a']));
				
				$method = strtolower($_SERVER['REQUEST_METHOD']);

				self::$controllerName = ucwords(self::$controllerPath) . "Controller";
				
				self::$fileName = BASE_PATH . 'controllers/'. self::$controllerPath . '.php';

				if(file_exists(self::$fileName)){			
					require_once(self::$fileName);

					if(class_exists(self::$controllerName)){

						self::$controller = new self::$controllerName();
						$action = $method.ucwords(self::$actionName);

						if (is_callable(array(self::$controller , $action)))
						{	
							View::$currentController = self::$controllerPath;
							View::$currentAction = self::$actionName;

							if(self::$controller->requireAuth())
					        {
					        	self::$controller->applyAuth($action);					        	
					        }else
					        {					        	
					        	self::$controller->$action();
					        }
						}else
						{
							self::$controller->layout = "";
							self::$controller->notFound();									
						}
					}else
					{
						require_once(BASE_PATH . 'controllers/base.php');
						$baseController = new BaseController();
						$baseController->notFound();
					}
				}else
				{
					require_once(BASE_PATH . 'controllers/base.php');
					$baseController = new BaseController();
					$baseController->notFound();					
				}
						
		} catch (Exception $e) {
			if(!Config::get('app::development'))
			{				
				View::make('error/apperror', 
					   array('message'=>$e->getMessage())
				);
				exit;
			}else{
				 throw new Exception($e->getMessage());
			}	
		}			
	}	

	public static function getControllerName()
	{
		return self::$controllerName;
	}

	public static function getActionName()
	{
		return self::$actionName;
	}

	public static function getController()
	{
		return self::$controller;
	}

	public static function getFileName()
	{
		return self::$fileName;
	}

	public static function getControllerPath()
	{
		return self::$controllerPath;
	}

	public static function redirect($path=null)
	{
		if(!is_null($path))		{
			header('Location: ' . $path);
		}
	}

	public static function redirectToAction($controller = null, $action = null, $params = array())
	{
		if(!is_null($controller))
		{
			if(is_null($action))
					$action = 'index';

			$url = "/?c=$controller&a=$action";
			
			foreach ($params as $token => $value) {
				$url .= '&'.trim($token) . '=' . trim($value); 
			}

			header("Location: " . $url);
		}
	}
}