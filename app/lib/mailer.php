<?php

class Mailer
{

  protected $domain;
  protected $apiKey;
  protected $contentType;

  public function __construct($domain, $apiKey, $contentType = 'html')
  {
    $this->apiKey = $apiKey;
    $this->domain = $domain;
    $this->contentType = $contentType;
  }

  public function send($body, $from, $to, $subject = 'Anacasti Notifications')
  {
    try{
      session_write_close(); 

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_USERPWD, 'api:'.$this->apiKey);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v2/'.$this->domain.'.mailgun.org/messages');
      curl_setopt($ch, CURLOPT_POSTFIELDS, array('from' => trim($from).'@'.$this->domain.'.mailgun.org>',
       'to' => $to,
       'subject' => $subject,
       $this->contentType => $body));

      $result = curl_exec($ch);
      curl_close($ch);

      $response = json_decode($result);
      // print_r($response);
      if(isset($response->id)){  
        if(strlen($response->id) > 0 && $response->message = "Queued. Thank you."){
          return true;
        }
      }

    }catch(Exception $ex){
      print($ex->getMessage());
    } 

    return false;
  }

}
