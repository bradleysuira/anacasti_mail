<?php
class Config
{	
	protected static $values = array();

	public static function run()
	{
		self::$values['app'] = include BASE_PATH . 'config/app.php';		
		self::$values['mail'] = include BASE_PATH . 'config/mail.php';		
		self::$values['database'] = include BASE_PATH . 'config/database.php';
		self::$values['views'] = include BASE_PATH . 'config/views.php';				
	}

	public static function get($key)
	{		
		$paths = explode("::",$key);
		if(array_key_exists($paths[0], self::$values))
		{			
			if(array_key_exists($paths[1], self::$values[$paths[0]]))
			{
				return self::$values[$paths[0]][$paths[1]];
			}
		}

		return null;
	}
}

