<?php

class DB
{
	

	protected static $connection = null;	
	protected static $queries = array();
	/**
	 * The default PDO connection options.
	 *
	 * @var array
	 */
	protected static $options = array(
		PDO::ATTR_CASE => PDO::CASE_NATURAL,
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL,
		PDO::ATTR_STRINGIFY_FETCHES => false,
		PDO::ATTR_EMULATE_PREPARES => false,
		PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true
		);


	/**
	 * Create a new PDO connection if not exists
	 *
	 * @param  string  $dsn
	 * @param  array   $config
	 * @param  array   $options
	 * @return PDO
	 */
	public static function lastQueries()
	{
		return self::$queries;
	}

	public static function connection()
	{
		if(is_null(self::$connection))
		{
				$host = Config::get('database::host');
				$dbname = Config::get('database::db_name');
				$port = Config::get('database::port');		
				$dsn = "mysql:host={$host};dbname={$dbname}";
				$username = Config::get('database::username');
				$password = Config::get('database::password');

				self::$connection = new PDO($dsn, $username, $password, self::$options);

				$collation = Config::get('database::collation');
				$charset = Config::get('database::charset');
				$names = "set names '$charset' collate '$collation'";				
				self::$connection->prepare($names)->execute();
				self::$queries[] = $names;
		}

		return self::$connection;
	}

	public static function query($query, $values = array(), $fetch = null)
	{
		if(is_null($fetch))
			$fetch = Config::get('database::fetch');

		$stmt = self::connection()->prepare($query);			
		$result = $stmt->execute($values); 
		self::$queries[] = $stmt->queryString;

		if($result)
			return $stmt->fetchAll($fetch);
		else
			throw new Exception("Query failed with message" . $stmt->errorInfo()[2], 1);	
	}

	public static function count($query, $values = array())
	{
		$stmt = self::connection()->prepare($query);
		self::$queries[] = $stmt->queryString;

		$result = $stmt->execute($values); 		

		if($result)
			return $stmt->fetch(PDO::FETCH_NUM)[0];
		else
			throw new Exception("Query failed with message" . $stmt->errorInfo()[2], 1);	
	}	

	public static function update($query, $values = array())
	{		
		$stmt = self::connection()->prepare($query);
		self::$queries[] = $stmt->queryString;			
		$result = $stmt->execute($values); 
		//$stmt->debugDumpParams();
		//echo '<br><pre>'. var_dump($stmt). '</pre>';
		if($result)
			return $stmt->rowCount();
		else
			throw new Exception("Query failed with message" .$stmt->errorInfo()[2], 1);
	}

	public static function delete($query, $values = array())
	{
		return self::update($query, $values);
	}

	public static function insert($query, $values)
	{
		$stmt = self::connection()->prepare($query);
		self::$queries[] = $stmt->queryString;
		$result = $stmt->execute($values); 				
		if($result)
			return self::connection()->lastInsertId();
		else
			throw new Exception("Query failed with message" . $stmt->errorInfo()[2], 1);	
	}

}