<?php
class View
{
	protected static $userIsAuth = false;
	public static $currentController;
	public static $currentAction;

	public static function make($name, array $data = null)
	{	
		$controller = Dispatcher::getController();
		self::$userIsAuth = $controller->userIsAuthenticated();
		
		if(!is_null($data) && !array_key_exists('errors', $data))
			$data['errors'] = array();

		if(!is_null($data) && !array_key_exists('messages', $data))
			$data['messages'] = "";

		$data = (object)$data;			
			if(!isset($data->title))
				$data->title = ucfirst(Dispatcher::getControllerPath())
							  . '-'. ucfirst(Dispatcher::getActionName());

		$basePath = BASE_PATH . Config::get('views::path');		
			$fileName  = $basePath . strtolower(trim($name)) . '.php';
			if(!file_exists($fileName )){
				echo 'View not exist ' . $fileName;
				exit();
			}

		if($controller->useLayout())
		{

			Layout::begin($controller->getLayout(), $data);
			include $fileName;
			Layout::end($controller->getLayout(), $data);
		}
		else
		{	
			include $fileName;
		}
	}

}