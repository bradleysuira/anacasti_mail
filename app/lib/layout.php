<?php
class Layout
{

	protected static function get($layout, $part, $data = null)
	{			
		$basePath = BASE_PATH . Config::get('views::path');		
			$layoutPart  = $basePath . strtolower(trim($layout)) . '_'.trim($part).'.php';
			if(!file_exists($layoutPart )){
				echo 'error loading layout ' . $layoutPart;
				exit();
			}

			include $layoutPart;
	}

	public static function begin($layout, $data = null)
	{			
		self::get($layout,'begin',$data);
	}	

	public static function end($layout, $data = null)
	{			
		self::get($layout,'end',$data);
	}	
}