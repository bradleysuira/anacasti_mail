

<?php 
class Paginator { 


    public static function make($per_page = 10, $page = 1, $total, $url = null){ 
        if(is_null($url))
        {
            $url = "/?";
            foreach($_GET as $key => $value)
            {
             if($key!="page")
             {
                $url .= $key . "=" . $value;
            }
        }
        $url .= "&page=";
    }
    $adjacents = "2";

    $page = ($page == 0 ? 1 : $page); 
    $start = ($page - 1) * $per_page; 
    $first = 1;
    $prev = $page - 1; 
    $next = $page + 1;
    $lastpage = ceil($total/$per_page);
    $lpm1 = $lastpage - 1;

    $pagination = "";
    if($lastpage > 1)
    { 
        $pagination .= '<ul class="uk-pagination uk-pagination-left">';
        $pagination .= "<li class='uk-alert-success'>Página $page de $lastpage </li><li></li> ";
        if ($page > 1){
            $pagination.= "<li><a href='{$url}$first'><i class='uk-icon-double-angle-left'></i></a></li>";
            $pagination.= "<li><a href='{$url}$prev'><i class='uk-icon-angle-left'></i></a></li>";           
             
        }else
        {
            $pagination.= "<li class='uk-disabled'><span><i class='uk-icon-double-angle-left'></i></span></li>";
            $pagination.= "<li class='uk-disabled'><span><i class='uk-icon-angle-left'></i></span></li>";
        }
        if ($lastpage < 7 + ($adjacents * 2))
        { 
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<li class='uk-active'><span>$counter</span></li>";
                else
                    $pagination.= "<li class='uk-item-n'><a href='{$url}$counter'>$counter</a></li>"; 
            }
        }
        elseif($lastpage > 5 + ($adjacents * 2))
        {
            if($page < 1 + ($adjacents * 2)) 
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li class='uk-active'><span>$counter</span></li>";
                    else
                        $pagination.= "<li ><a href='{$url}$counter'>$counter</a></li>"; 
                }
                $pagination.= "<li class='dot'>...</li>";
                $pagination.= "<li><a href='{$url}$lpm1'>$lpm1</a></li>";
                $pagination.= "<li><a href='{$url}$lastpage'>$lastpage</a></li>"; 
            }
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination.= "<li><a href='{$url}1'>1</a></li>";
                $pagination.= "<li><a href='{$url}2'>2</a></li>";
                $pagination.= "<li class='dot'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li class='uk-active'><span>$counter</span></li>";
                    else
                        $pagination.= "<li ><a href='{$url}$counter'>$counter</a></li>"; 
                }
                $pagination.= "<li class='dot'>..</li>";
                $pagination.= "<li><a href='{$url}$lpm1'>$lpm1</a></li>";
                $pagination.= "<li><a href='{$url}$lastpage'>$lastpage</a></li>"; 
            }
            else
            {
                $pagination.= "<li><a href='{$url}1'>1</a></li>";
                $pagination.= "<li><a href='{$url}2'>2</a></li>";
                $pagination.= "<li class='dot'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li class='uk-active'><span>$counter</span></li>";
                    else
                        $pagination.= "<li><a href='{$url}$counter'>$counter</a></li>"; 
                }
            }
        }        

        if ($page < $counter - 1){
            $pagination.= "<li><a href='{$url}$next'><i class='uk-icon-angle-right'></i></a></li>";
            $pagination.= "<li><a href='{$url}$lastpage'><i class='uk-icon-double-angle-right'></i></a></li>";
// $pagination.= "<li><a href='{$url}$lastpage'>Last</a></li>";
        }else{
            $pagination.= "<li class='uk-disabled'><span><i class='uk-icon-angle-right'></i></span></li>";
            $pagination.= "<li class='uk-disabled'><span><i class='uk-icon-double-angle-right'></i></span></li>";
        }
        $pagination.= "</ul>\n"; 
    } 
    return $pagination;
} 

}